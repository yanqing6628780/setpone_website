<?php
/*
Template Name: Portfolio Template
*/
?>
<?php get_header();?>

            <!-- BEGIN OF PAGE TITLE -->
            <div id="header-inner">
                <h1><?php the_title();?></h1>
                <?php 
                global $post;
                $page_heading_icon = get_post_meta($post->ID, '_page_heading_icon', true );
                if ($page_heading_icon !="") { ?>
                  <img src="<?php echo $page_heading_icon;?>"  alt="" class="imgtitle" />
                <?php } else {
                  switch_image_heading();
                }
                ?>
            </div>
            <!-- END OF PAGE TITLE -->
            
            <!-- BEGIN OF CONTENT -->
            <div id="content-inner">
                <!-- begin of col-920 (services) -->
                <div class="col-920 fullwidth">                	
                  <?php $portfolio_desc = get_option('epsilon_portfolio_desc');?>
                  <p class="italictext"><?php echo stripslashes($portfolio_desc);?></p>
                  <!-- begin of col-900 (services) -->
                    <div class="col-900">
                    
                    <ul id="filter">
                    <?php
              			 $portfolio_page = get_option('epsilon_portfolio_page');
              			 $portfolio_pid = get_page_by_title($portfolio_page);
              			 ?>
           			      <li><a class="<?php if (!get_query_var('portfolio_category')) echo 'current'; ?>" href="<?php echo get_page_link($portfolio_pid->ID);?>"><?php echo __('All','ecobiz');?></a></li>
                    	<?php  
                      $categories = get_categories('taxonomy=portfolio_category&orderby=ID&title_li=&hide_empty=0');
                      foreach ($categories as $category) { 
                      $termlink = get_term_link($category->slug,$category->taxonomy);
                      ?>
                        <li><a  class="<?php if (get_query_var($category->taxonomy) == $category->slug) echo 'current'; ?>" href="<?php echo $termlink;?>"><?php echo $category->name;?></a></li>
                        <?php
                      }
                      ?>
                    </ul>
                  <?php
                    $page = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $portfolio_items_num  = (get_option('epsilon_portfolio_items_num')) ? get_option('epsilon_portfolio_items_num') : 4; 
                    $portfolio_order = (get_option('epsilon_portfolio_order')) ? get_option('epsilon_portfolio_order') : "date";
                                        
                    query_posts(array( 'post_type' => 'portfolio', 'showposts' => $portfolio_items_num,'paged'=>$page,"orderby" => $portfolio_order,'order'=> 'ASC'));
                    
                    $counter = 0;
                    while ( have_posts() ) : the_post();
                    $counter++;

                    $image_thumbnail = get_post_meta($post->ID, '_image_thumbnail', true );
                    $portfolio_link = get_post_meta($post->ID, '_portfolio_link', true );
                    $pf_link = get_post_meta($post->ID, '_portfolio_link', true );
                    $pf_url = get_post_meta($post->ID, '_portfolio_url', true );
                  ?>                
                	
                    <div class="col-280 boxpf">
                    	<a href="<?php echo ($pf_link) ? $pf_link : thumb_url();?>" rel="prettyPhoto[pf]" >
                        <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
                          <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo thumb_url();?>&amp;h=128&amp;w=280&amp;zc=1" alt=""/>                
                        <?php } else { ?>
                        <?php if ($image_thumbnail)  { ?>			
                        <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo $image_thumbnail;?>&amp;h=128&amp;w=280&amp;zc=1" alt="" />
                        <?php }
                          }
                        ?>
                        </a>
                        <h4><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
                    	<p><?php the_excerpt();?></p>
                    </div>
                     <?php if ($counter % 3 !=0 ) echo '<div class="vertical-separator-noline"></div>';?>
                     <?php endwhile;?>
                    <div class="pages pfpages">
                    <?php 
                		if (function_exists('wp_pagenavi')) :
                		    wp_pagenavi();
                		  else : 
                		?>
                  		<div class="navigation">
                        <div class="alignleft"><?php next_posts_link(__('&laquo; Previous Entries','epsilon')) ?></div>
                        <div class="alignright"><?php previous_posts_link(__('Next Entries &raquo;','epsilon')) ?></div>
                        <div class="clear"></div>
                  		</div>
                    <?php endif;?>  
                    </div>
                    </div>
                     <!-- end of col-900 (services) -->
                </div>
                <!-- end of col-920 (services) -->
            </div>
            <!-- END OF CONTENT -->
        </div>
        <!-- end of main -->
        
<?php get_footer();?>