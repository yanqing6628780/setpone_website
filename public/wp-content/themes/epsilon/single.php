<?php get_header();?>

            <!-- BEGIN OF PAGE TITLE -->
            <div id="header-inner">
                <h1>
                  <?php
                    $blog_cats_include = get_option('epsilon_blog_cats_include');
                    if(is_array($blog_cats_include)) {
                      $blog_include = implode(",",$blog_cats_include);
                    }   
                    $testimonial_cat = get_option('epsilon_testimonial_cid');
                    
                    if (in_category($blog_include)) { 
                      echo __("Blog",'epsilon'); 
                    }              
                  ?>                   
                </h1>
                <?php
                  $blog_page = get_option('epsilon_blog_page');
                  $blog_page_id = get_page_by_title($blog_page);
                  $page_heading_icon = get_post_meta($blog_page_id->ID, '_page_heading_icon', true );
                  if ($page_heading_icon !="") { ?>
                    <img src="<?php echo $page_heading_icon;?>"  alt="" class="imgtitle" />
                  <?php } else {
                    switch_image_heading();
                  }
                ?>
            </div>
            <!-- END OF PAGE TITLE -->
            
            <!-- BEGIN OF CONTENT -->
            <div id="content-inner-sidebar">
                	 <!-- begin of col-620  -->
                	<div class="col-620">
                    	<div id="maintext">
                      	<div class="post">
                    	 <?php if (have_posts()) : while (have_posts()) : the_post();?>
                    	 <?php
                        $image_thumbnail = get_post_meta($post->ID,"_image_thumbnail",true);
                        $portfolio_link = get_post_meta($post->ID,"_portfolio_link",true);                      	 
                    	 ?>
                        	<h3><?php the_title();?></h3>
                            <div class="metadata">
                              <?php echo __('Posted by ','epsilon');?><?php the_author_posts_link();?> <?php echo __('on ','epsilon');?><?php the_time('F d, Y');?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php comments_popup_link(__('0 Comment','epsilon'),__('1 Comment','epsilon'),__('% Comments','epsilon'));?>
                            </div>
                            <div class="entry">
                            <?php $featured_image = get_option('epsilon_featured_image');
                            if ($featured_image != "true") { 
                                if ($portfolio_link) {
                                  if (is_youtube($portfolio_link)) { ?>
                                    <div class="movie_container"><a href="<?php echo $portfolio_link;?>"  rel="youtube"></a></div>
                                  <?php
                                  } else if (is_vimeo($portfolio_link)) { ?>
                                    <div class="movie_container"><a href="<?php echo $portfolio_link;?>"  rel="vimeo"></a></div>    
                                  <?php  
                                  } else if (is_quicktime($portfolio_link)) { 
                                    ?>
                                    <div class="movie_container"><a href="<?php echo $portfolio_link;?>"  rel="quicktime"></a></div>
                                    <?php
                                  } else  if (is_flash($portfolio_link)) { ?>
                                    <div class="movie_container"><a href="<?php echo $portfolio_link;?>"  rel="flash"></a></div>
                                    <?php
                                  } else { ?>
                                    <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
                                      <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo thumb_url();?>&amp;h=180&amp;w=585&amp;zc=1" alt=""/>                
                                    <?php } else { ?>
                                    <?php if ($image_thumbnail)  { ?>			
                                      <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo $image_thumbnail;?>&amp;h=180&amp;w=585&amp;zc=1" alt="" />
                                    <?php }
                                      }
                                    }
                                  } else { ?>
                                    <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
                                    <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo thumb_url();?>&amp;h=180&amp;w=585&amp;zc=1" alt=""/>                
                                  <?php } else { ?>
                                  <?php if ($image_thumbnail)  { ?>			
                                    <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo $image_thumbnail;?>&amp;h=180&amp;w=585&amp;zc=1" alt="" />
                                  <?php }
                                    }           
                                }
                              }
                              ?>                              
                            	 <?php the_content();?>

                            </div>
                          	<!-- begin of comment -->
                              <?php $disable_comment = get_option('epsilon_disable_comment'); ?>
                              <?php 
                              if ($disable_comment !="true") {
                                comments_template('', true);  
                              }
                              ?>
                            <!-- end of comment -->                            
                          <?php endwhile;endif;?>                                
                        </div><!-- end of post -->                    	

                      </div><!-- end of maintext -->
                    </div>
                    <!-- end of col-620 -->
                    
                    <?php get_sidebar();?>
                    
                    <div class="clear"></div>
            </div>
            <div id="content-inner-sidebar-bottom"></div>
            <!-- END OF CONTENT -->
        </div>
        <!-- end of main -->
        
<?php get_footer();?>