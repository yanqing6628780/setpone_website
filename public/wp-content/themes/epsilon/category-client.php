<?php get_header();?>

            <!-- BEGIN OF PAGE TITLE -->
            <div id="header-inner">
                <h1><?php single_cat_title();?></h1>
                <?php
                  $blog_page = get_option('epsilon_blog_page');
                  $blog_page_id = get_page_by_title($blog_page);
                  $page_heading_icon = get_post_meta($blog_page_id->ID, '_page_heading_icon', true );
                  if ($page_heading_icon !="") { ?>
                    <img src="<?php echo $page_heading_icon;?>"  alt="" class="imgtitle" />
                  <?php } else {
                    switch_image_heading();
                  }
                ?>
            </div>
            <!-- END OF PAGE TITLE -->
            
            <!-- BEGIN OF CONTENT -->
            <div id="content-inner-sidebar">
                	 <!-- begin of col-620  -->
                	<div class="col-620">
                    	<div id="maintext">
                          <?php
                          
                          $numtext = (get_option('epsilon_blogtext')) ? get_option('epsilon_blogtext') : 60;
                          while ( have_posts() ) : the_post();
                          $image_thumbnail = get_post_meta($post->ID, '_image_thumbnail', true );
                        	?>        
                    	
                        	 <div class="post">
                            	<h3><?php the_title();?></h3>
                                <div class="entry">
                                <?php if ($image_thumbnail)  : ?>			
                                  <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo $image_thumbnail;?>&amp;h=85&amp;w=95&amp;zc=1" alt="" class="imgleft"/>
                                  <?php else : ?>
                                  <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php  echo get_template_directory_uri();?>/images/img-client.gif&amp;h=85&amp;w=95&amp;zc=1" alt=""  class="imgleft"/>
                                  <?php endif;?>    
                                  <?php the_content();?>
                                </div>
                            </div><!-- end of post -->
                            <?php endwhile;?>
                            <div class="pages blogpages">
                              <?php 
                          		if (function_exists('wp_pagenavi')) :
                          		    wp_pagenavi();
                          		  else : 
                          		?>
                            		<div class="navigation">
                            			<div class="alignleft"><?php next_posts_link(__('&laquo; Previous Entries','epsilon')) ?></div>
                            			<div class="alignright"><?php previous_posts_link(__('Next Entries &raquo;','epsilon')) ?></div>
                            			<div class="clear"></div>
                            		</div>
                              <?php endif;?>                              
                            </div>
                        </div><!-- end of maintext -->
                    </div>
                    <!-- end of col-620 -->
                    
                    <?php get_sidebar();?>
                    
                    <div class="clear"></div>
            </div>
            <div id="content-inner-sidebar-bottom"></div>
            <!-- END OF CONTENT -->
        </div>
        <!-- end of main -->
        
<?php get_footer();?>