            <!-- begin of header -->
            <div id="header">            
            	<!-- BEGIN OF SLIDESHOW -->
            	<ul id="slideshow">
                
                  <?php
                    $slideshow_order = get_option('epsilon_slideshow_order') ? get_option('epsilon_slideshow_order') : "date";
                    
                    query_posts(array( 'post_type' => 'slideshow', 'showposts' => -1, 'orderby' => $slideshow_order)); 
                      while (have_posts() ) : the_post(); 
                      $slideshow_url = (get_post_meta($post->ID, '_slideshow_url', true )); 
                      $signup_url = get_post_meta($post->ID, '_custom_slideshow_url', true );
                    ?>    
                    <li>
                        <div class="header-left">
                            <div class="header-text">
                                <h1><?php the_title();?></h1>
                                <?php the_content();?>
                                <div class="conbut">
                                  <?php if ($slideshow_url !="") { ?>
                                	<a class="button" href="<?php echo $slideshow_url;?>"><?php echo __('Details','epsilon');?></a>
                                  <?php } ?>
                                  <?php if ($signup_url !="") { ?>
                                  <a class="button" href="<?php echo $signup_url;?>"><?php echo __('Sign Up','epsilon');?></a>
                                  <?php } ?>
                                </div>
                           </div>
                        </div>
                        <div class="header-right">
                            <div class="header-image">
                              <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
                                <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo thumb_url();?>&amp;h=342&amp;w=424&amp;zc=1" alt=""/>                
                              <?php } ?>
                           </div>
                        </div>
                    </li>
                  <?php endwhile;?>
                </ul>
                <!-- END OF SLIDESHOW -->
            </div>
            <!-- end of header -->