<?php
/*
Template Name: Blog Template
*/
?>
<?php get_header();?>

            <!-- BEGIN OF PAGE TITLE -->
            <div id="header-inner">
                <h1><?php the_title();?></h1>
                <?php 
                global $post;
                $page_heading_icon = get_post_meta($post->ID, '_page_heading_icon', true );
                if ($page_heading_icon !="") { ?>
                  <img src="<?php echo $page_heading_icon;?>"  alt="" class="imgtitle" />
                <?php } else {
                  switch_image_heading();
                }
                ?>
            </div>
            <!-- END OF PAGE TITLE -->
            
            <!-- BEGIN OF CONTENT -->
            <div id="content-inner-sidebar">
                	 <!-- begin of col-620  -->
                	<div class="col-620">
                    	<div id="maintext">
                          <?php
                          $blog_cats_include = get_option('epsilon_blog_cats_include');
                          if(is_array($blog_cats_include)) {
                            $blog_include = implode(",",$blog_cats_include);
                          } 
                          
                          $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                          $blog_num = get_option('epsilon_blog_num') ? get_option('epsilon_blog_num') : get_option('posts_per_page');
                          $numtext = (get_option('epsilon_blogtext')) ? get_option('epsilon_blogtext') : 60;
                          query_posts("cat=$blog_include&showposts=$blog_num&paged=$paged");
                          while ( have_posts() ) : the_post();
                          $image_thumbnail = get_post_meta($post->ID, '_image_thumbnail', true );
                        	?>        
                    	
                        	 <div class="post">
                            	<h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                                <div class="metadata">
                                  <?php echo __('Posted by ','epsilon');?><?php the_author_posts_link();?> <?php echo __('on ','epsilon');?><?php the_time('F d, Y');?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php comments_popup_link(__('0 Comment','epsilon'),__('1 Comment','epsilon'),__('% Comments','epsilon'));?>
                                </div>
                                <div class="entry">
                                <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
                                  <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo thumb_url();?>&amp;h=180&amp;w=585&amp;zc=1" alt=""/>                
                                <?php } else { ?>
                                <?php if ($image_thumbnail)  { ?>			
                                  <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo $image_thumbnail;?>&amp;h=180&amp;w=585&amp;zc=1" alt="" />
                                <?php }
                                  }
                                ?>    
                                <p><?php excerpt($numtext);?></p>
                                <div class="butmore"><a class="more" href="<?php the_permalink();?>"><?php echo __('Read more','epsilon');?></a></div>
                                </div>
                            </div><!-- end of post -->
                            <?php endwhile;?>
                            
                            <div class="pages blogpages">
                              <?php 
                          		if (function_exists('wp_pagenavi')) :
                          		    wp_pagenavi();
                          		  else : 
                          		?>
                            		<div class="navigation">
                            			<div class="alignleft"><?php next_posts_link(__('&laquo; Previous Entries','epsilon')) ?></div>
                            			<div class="alignright"><?php previous_posts_link(__('Next Entries &raquo;','epsilon')) ?></div>
                            			<div class="clear"></div>
                            		</div>
                              <?php endif;?>                              
                            </div>
                        </div><!-- end of maintext -->
                    </div>
                    <!-- end of col-620 -->
                    <?php wp_reset_query();?>
                    <?php get_sidebar();?>
                    
                    <div class="clear"></div>
            </div>
            <div id="content-inner-sidebar-bottom"></div>
            <!-- END OF CONTENT -->
        </div>
        <!-- end of main -->
        
<?php get_footer();?>