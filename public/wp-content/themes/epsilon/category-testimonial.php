<?php get_header();?>

            <!-- BEGIN OF PAGE TITLE -->
            <div id="header-inner">
                <h1><?php single_cat_title();?></h1>
                <?php switch_image_heading();?>
            </div>
            <!-- END OF PAGE TITLE -->
            
            <!-- BEGIN OF CONTENT -->
            <div id="content-inner-sidebar">
                	 <!-- begin of col-620  -->
                	<div class="col-620">
                    	<div id="maintext">
                    	<?php if (have_posts()) : while (have_posts()) : the_post();?>
                    	
                         <div class="quote">
                            <blockquote>
                            <div><?php the_content();?></div>
                            </blockquote>
                            <p><strong><?php the_title();?></strong></p>
                          </div>
                    	
                    	<?php endwhile;endif;?>
                        
                      </div><!-- end of maintext -->
                    </div>
                    <!-- end of col-620 -->
                    
                    <?php get_sidebar();?>
                    
                    <div class="clear"></div>
            </div>
            <div id="content-inner-sidebar-bottom"></div>
            <!-- END OF CONTENT -->
        </div>
        <!-- end of main -->
        
<?php get_footer();?>