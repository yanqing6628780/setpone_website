<?php get_header();?>

            <!-- BEGIN OF PAGE TITLE -->
            <div id="header-inner">
                <h1><?php the_title();?></h1>
                <?php switch_image_heading();?>
            </div>
            <!-- END OF PAGE TITLE -->
            
            <!-- BEGIN OF CONTENT -->
            <div id="content-inner-sidebar">
                	 <!-- begin of col-620  -->
                	<div class="col-620">
                    	<div id="maintext">
                	      <?php
                        $_404_text = (get_option('epsilon_404_text')) ? get_option('epsilon_404_text') : __("Apologies, but the page you requested could not be found",'epsilon');
                      ?>
                        <h3><?php echo stripslashes($_404_text);?></h3>
                        <?php get_search_form();?>
                        
                      </div><!-- end of maintext -->
                    </div>
                    <!-- end of col-620 -->
                    
                    <?php get_sidebar();?>
                    
                    <div class="clear"></div>
            </div>
            <div id="content-inner-sidebar-bottom"></div>
            <!-- END OF CONTENT -->
        </div>
        <!-- end of main -->
        
<?php get_footer();?>