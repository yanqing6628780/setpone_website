        <div id="main-curve-bottom"></div>
        
    	<!-- BEGIN OF FOOTER -->
    	<div id="footer">
        	<div id="footer-col">
            	 <!-- begin of col-920 (footer column) -->
                  <div class="col-920">
                  	<div class="col-196 equal">
                      <?php $footer_logo = get_option('epsilon_footerlogo');?>
                      <img src="<?php echo ($footer_logo) ? $footer_logo : get_template_directory_uri().'/images/logo-footer.png';?>" alt="" />
                    </div>
                    <div class="vertical-separator equal"></div>
                    <div class="col-160 equal">
                      <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Bottom1')) : ?>
                      <div class="footer-widget">
                        <ul>
                          <?php
                          $excludeinclude_pages = get_option('epsilon_excludeinclude_pages');
                            if(is_array($excludeinclude_pages)) {
                              $page_exclusions = implode(",",$excludeinclude_pages);
                            }
                          ?>                        
                          <?php wp_list_pages('title_li=&depth=1&exclude='.$page_exclusions); ?>
                        </ul>
                      </div>
                      <?php endif;?>
                    </div>
                    <div class="vertical-separator equal"></div>
                    <div class="col-160 equal">
                      <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Bottom2')) : ?>
                      <div class="footer-widget">
                        <ul>
                          <?php wp_list_bookmarks('title_li=&categorize=0'); ?>
                        </ul>
                      </div>
                      <?php endif;?>
                    </div>
                    <div class="vertical-separator equal"></div>
                    <div class="col-284 equal">
                      <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Bottom3')) : ?>
                        <?php include (TEMPLATEPATH.'/social-links.php');?>
                      <?php endif;?>
                    </div>
                  	<div class="clear"></div>
                  </div>
                 <!-- end of col-920 (footer column) -->
            </div>
            <div id="footer-text">
            <?php $footer_text = get_option('epsilon_footer_text');?>
            <?php echo ($footer_text) ? stripslashes($footer_text) : "Copyright &copy; 2010 Epsilon Business Corp. All rights reserved";?>
            </div>
        </div>
        <!-- END OF FOOTER -->
        
    </div>
    <!-- end of container 960px center -->
        
    <?php include (TEMPLATEPATH.'/popup-form.php');?>
    
    <script type="text/javascript"> Cufon.now(); </script>
    <?php 
    $ga_code = get_option('epsilon_ga_code');
    if ($ga_code) echo stripslashes($ga_code);
    ?> 
    
    <?php wp_footer();?>
	
</body>
</html>