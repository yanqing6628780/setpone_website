<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>"  />
<title><?php if (is_home () ) { bloginfo('name'); echo " - "; bloginfo('description'); 
} elseif (is_category() ) {single_cat_title(); echo " - "; bloginfo('name');
} elseif (is_single() || is_page() ) {single_post_title(); echo " - "; bloginfo('name');
} elseif (is_search() ) {bloginfo('name'); echo " search results: "; echo esc_html($s);
} else { wp_title('',true); }?></title>
<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
<meta name="robots" content="follow, all" />
<?php $favico = get_option('epsilon_custom_favicon');?>
<link rel="shortcut icon" href="<?php echo ($favico) ? $favico : get_template_directory_uri().'/images/favicon.ico';?>"/>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php wp_head(); ?>	

<!-- ////////////////////////////////// -->
<!-- //      Javascript Files        // -->
<!-- ////////////////////////////////// -->
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/cufon-yui.js"></script>
<?php $cufon_fonts = get_option('epsilon_cufon_fonts'); if ($cufon_fonts == "") $cufon_fonts = "Vegur_400-Vegur_700.font.js";?>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/fonts/<?php echo $cufon_fonts;?>"></script>
<script type="text/javascript">
    Cufon.set('fontWeight','normal').replace('h1') ('h2') ('h3') ('h4') ('h5') ('h6')('.pp_content .signup-content h2') ('.boldtext-medium') ('.boldtext-strong') ('.pullquote_left') ('.pullquote_right')('.boxtitle-hosting',{ignoreClass :'.currency'}) 
	('#menu li a', { 
		hover: true,
		color: '#02253d',
		textShadow: '0px 1px 1px #ffffff'
	 })
 	('.button') ('.button2') ('.button-red')('.button-blue')('.submit-popup', { 
		hover: true,
		textShadow: '0px 1px 0px #ededed'
	 })
	;
</script>
<script type="text/javascript"> 
  <?php
    $slideshow_speed  = get_option('epsilon_slideshow_speed');
    $slideshow_auto = get_option('epsilon_slideshow_auto');
    $slideshow_pager  = get_option('epsilon_slideshow_pager');
    $slideshow_controls = get_option('epsilon_slideshow_controls');
    $slideshow_loop = get_option('epsilon_slideshow_loop');
    $slideshow_hidecontrol = get_option('epsilon_slideshow_hidecontrol');
    $slideshow_pause = get_option('epsilon_slideshow_pause');
  ?> 
  
  jQuery(document).ready(function($) {
    
    var numImgs = $('#slideshow').find(".header-image img").length;
    if (numImgs > 0) {
			$('#slideshow').bxSlider({
        speed: <?php echo ($slideshow_speed) ? $slideshow_speed : "700";?>,
        pause: <?php echo ($slideshow_pause) ? $slideshow_pause : '3000';?>,       
        auto: <?php echo ($slideshow_auto) ? $slideshow_auto : 'true';?>,
        pager: <?php echo $slideshow_pager ? $slideshow_pager : 'true';?>,
        controls: <?php echo ($slideshow_controls) ? $slideshow_controls : 'true';?>,
        infiniteLoop: false,
        mode: 'horizontal',
        hideControlOnEnd : <?php echo ($slideshow_hidecontrol) ? $slideshow_hidecontrol : 'false';?>
			});
    }
	});
</script>   
<!--[if lte IE 7]>  
	<link href="<?php echo get_template_directory_uri();?>/css/ie6.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/DD_belatedPNG.js"></script>
	<script type="text/javascript"> 
	   DD_belatedPNG.fix('img, .vertical-separator, #nav-slide, #pager a, .activeSlide'); 
	</script>    
<![endif]-->
<!--[if IE 7]>    
	<style type="text/css">
    .img-tour{width:106px; height:107px;}
    .ie7line{height:285px;}
    .popup-form{margin-left:25px;}
    .popup{margin-left:24px;}
	.submit-popup{margin-left:110px;}
	.textarea-popup{margin-left:25px;}
    </style>
<![endif]-->
<!--[if IE 8]>    
	<style type="text/css">
    .nav-testimonial{width:15%;height:21px;top:68px;left:210px;position:absolute;}
    .next{position:absolute;top:10px;left:22px;}
    .prev{position:absolute;top:10px;}
    </style>
<![endif]-->
</head>

<body>
	<!-- begin of container 960px center -->
	<div id="container">
    
    	<!-- begin of topnavigation -->
    	<div id="topnavigation">
        	<span class="toplink"><a href="#" class="signup-form"><?php echo __('Sign up','epsilon');?></a>|
          <?php 
            global $user_ID; 
            if ($user_ID): 
              echo '<a href="'.wp_logout_url(get_permalink()).'">'.__('Logout','epsilon').'</a>'; 
            else : 
              echo '<a href="#" class="login-form">'.__('Login','epsilon').'</a>'; 
            endif;
          ?>            
          </span>
        </div>
        <!-- end of topnavigation -->
        
    	<div id="main-curve-top"></div>
    	<!-- begin of main -->
    	<div id="main">
        
            <!-- begin of top -->
            <div id="top">
                <div id="logo">
                  <?php 
                    $logo_url  = get_option('epsilon_logo');
                    $epsilon_style = get_option('epsilon_switch_to_business');
                  ?>
                  <a href="<?php echo home_url();?>"><img src="<?php if ($logo_url != "") {echo $logo_url;} else { 
                    if ($epsilon_style == "true") 
                      echo get_template_directory_uri().'/css/styles/blue/logo.png'; 
                    else   
                      echo get_template_directory_uri().'/images/logo.png';
                    }
                    ?>" alt="<?php bloginfo('blogname');?>" /></a>								                  
                </div>
                
		
                <div id="topright">
                	<div><b>Language: <a href="http://www.step-one.hk/">English</a> <a href="http://www.step-one.hk/cn/">中文</a></b><br>
					Contact Number: 3612 9060<br> 
					Fax: 8148 7065<br>
					Email: info@step-one.hk
					
					</div>
					<!--<span class="boldtext-medium"><?php echo __('Quick Contact','epsilon');?></span><br />
                    <?php $info_phone = get_option('epsilon_info_phone');?>
                    <span class="boldtext-strong"><?php echo ($info_phone) ? $info_phone : "+123 456 5532";?></span>-->
                </div>
            </div>
            <!-- end of top -->
            
            <!-- BEGIN OF MAINMENU -->
            <div id="mainmenu">
              <?php
                if (function_exists('wp_nav_menu')) { 
                  wp_nav_menu( array('menu_id'=>'menu', 'menu_class' => '', 'theme_location' => 'topnav','fallback_cb'=>'epsilon_topmenu_pages','sort_column' => 'menu_order', 'depth' =>3) );
                } else {  
                  epsilon_topmenu_pages();
                } ?>

            </div>
            <!-- END OF MAINMENU -->
            
        <?php if (is_home()) include (TEMPLATEPATH.'/slideshow.php');?>