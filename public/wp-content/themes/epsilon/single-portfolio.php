<?php get_header();?>

            <!-- BEGIN OF PAGE TITLE -->
            <div id="header-inner">
                <h1><?php echo __("Portfolio",'epsilon');?></h1>
                <?php
                  $portfolio_page = get_option('epsilon_portfolio_page');
  	              $portfolio_pid = get_page_by_title($portfolio_page);
                  $page_heading_icon = get_post_meta($portfolio_pid->ID,"_page_heading_icon",true);
                  if ($page_heading_icon !="") { ?>
                    <img src="<?php echo $page_heading_icon;?>"  alt="" class="imgtitle" />
                  <?php } else {
                    switch_image_heading();
                  }
                ?>
            </div>
            <!-- END OF PAGE TITLE -->
            
            <!-- BEGIN OF CONTENT -->
            <div id="content-inner-sidebar">
                	 <!-- begin of col-620  -->
                	<div class="col-620">
                    	<div id="maintext">
                      	<div class="post">
                    	 <?php if (have_posts()) : while (have_posts()) : the_post();?>
                    	 <?php
                        $image_thumbnail = get_post_meta($post->ID,"_image_thumbnail",true);
                        $portfolio_link = get_post_meta($post->ID,"_portfolio_link",true);                      	 
                    	 ?>
                        	<h3><?php the_title();?></h3>
                            <div class="entry">
                                <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
                                    <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo thumb_url();?>&amp;h=180&amp;w=585&amp;zc=1" alt=""/>                
                                  <?php } ?>
                            	 <?php the_content();?>
                               <div class="clear"></div>
                              <?php 
                              $portfolio_visitsite = get_option('epsilon_portfolio_visitsite');
                              $pf_url = get_post_meta($post->ID, '_portfolio_url', true );
                              if ($pf_url) { ?>
                                <a href="<?php echo $pf_url;?>" class="button2"><span><?php echo $portfolio_visitsite ? $portfolio_visitsite : __('Visit Site','epsilon');?></span></a>
                              <?php
                              }
                              ?>
                            </div>                            
                          <?php endwhile;endif;?>                                
                        </div><!-- end of post -->                    	

                      </div><!-- end of maintext -->
                    </div>
                    <!-- end of col-620 -->
                    
                    <?php get_sidebar();?>
                    
                    <div class="clear"></div>
            </div>
            <div id="content-inner-sidebar-bottom"></div>
            <!-- END OF CONTENT -->
        </div>
        <!-- end of main -->
        
<?php get_footer();?>