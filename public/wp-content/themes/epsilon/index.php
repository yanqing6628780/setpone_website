<?php get_header();?>
            
            <!-- BEGIN OF CONTENT -->
            <div id="content">
            
                <?php 
                
                $epsilon_style = get_option('epsilon_switch_to_business');
                $homebox_price1 = get_option('epsilon_homebox_price1');
                $homebox_title1 = get_option('epsilon_homebox_title1');
                $homebox_desc1  = get_option('epsilon_homebox_desc1');
                $homebox_desturl1 = get_option('epsilon_homebox_desturl1');
                $homebox_image1 = get_option('epsilon_homebox_image1');
                $homebox_price2 = get_option('epsilon_homebox_price2');
                $homebox_title2 = get_option('epsilon_homebox_title2');
                $homebox_desc2  = get_option('epsilon_homebox_desc2');
                $homebox_desturl2 = get_option('epsilon_homebox_desturl2');
                $homebox_image2 = get_option('epsilon_homebox_image2');
                $homebox_price3 = get_option('epsilon_homebox_price3');  
                $homebox_title3 = get_option('epsilon_homebox_title3');
                $homebox_desc3  = get_option('epsilon_homebox_desc3');
                $homebox_desturl3 = get_option('epsilon_homebox_desturl3');
                $homebox_image3 = get_option('epsilon_homebox_image3');   
                
                $currency = get_option('epsilon_currency') ? get_option('epsilon_currency') : "&#36;";             

                if (get_option('epsilon_switch_to_business') == "true") {
                  include (TEMPLATEPATH.'/business-homepage.php');
                } else {
                  include (TEMPLATEPATH.'/hosting-homepage.php'); 
                }
                ?>
                
                <hr class="double" />
                
                <!-- begin of content-col (welcome text and our client) -->
                <div id="content-col">
                	 <!-- begin of col-440 (welcome text) -->
                	<div class="col-440 equal">
                	 <?php
                	   $welcome_title = get_option('epsilon_welcome_title');
                	   $welcome_message = get_option('epsilon_welcome_message');
                	 ?>
                    	<h2><?php echo ($welcome_title) ? stripslashes($welcome_title) : "Welcome to <span class=\"redtext\">Epsilon</span> company";?></h2>
                        <p><?php echo ($welcome_message) ? stripslashes($welcome_message) : "Nam libero tempore, cum soluta nobis eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debi tis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus.";?></p>
                    </div>
                    <!-- end of col-440 (welcome text) -->
                    <div class="vertical-separator equal"></div>
                    <!-- begin of col-440 (our client) -->
                	<div class="col-440 equal">
                    <h2>Authorized reseller </h2>
					
					<img src="http://www.step-one.hk/wp-content/uploads/2013/01/bottom_logo2.png" />
					<!--<?php 
                      $client_cid = get_option('epsilon_client_cid');
                      $clientcid = get_cat_ID($client_cid);
                      epsilon_clientlist($clientcid,4,"<h2>Our Clients</h2>");
                      ?>-->
                  </div>
                    <!-- end of col-440 (our client) -->
                    <div class="clear"></div>
                </div>
                <!-- end of content-col (welcome text and our client) -->
                
            </div>
            <!-- END OF CONTENT -->
        </div>
        <!-- end of main -->
        
<?php get_footer();?>