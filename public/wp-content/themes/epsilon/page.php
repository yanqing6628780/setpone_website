<?php get_header();?>

            <!-- BEGIN OF PAGE TITLE -->
            <div id="header-inner">
                <h1><?php the_title();?></h1>
                <?php 
                global $post;
                $page_heading_icon = get_post_meta($post->ID, '_page_heading_icon', true );
                if ($page_heading_icon !="") { ?>
                  <img src="<?php echo $page_heading_icon;?>"  alt="" class="imgtitle" />
                <?php } else {
                  switch_image_heading();
                }
                ?>
            </div>
            <!-- END OF PAGE TITLE -->
            
            <!-- BEGIN OF CONTENT -->
            <div id="content-inner-sidebar">
                	 <!-- begin of col-620  -->
                	<div class="col-620">
                    	<div id="maintext">
                    	<?php if (have_posts()) : while (have_posts()) : the_post();?>
                    	
                    	 <?php the_content();?>
                    	
                    	<?php endwhile;endif;?>
                        
                      </div><!-- end of maintext -->
                    </div>
                    <!-- end of col-620 -->
                    
                    <?php get_sidebar();?>
                    
                    <div class="clear"></div>
            </div>
            <div id="content-inner-sidebar-bottom"></div>
            <!-- END OF CONTENT -->
        </div>
        <!-- end of main -->
        
<?php get_footer();?>