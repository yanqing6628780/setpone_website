<?php

/* List Styles */

function epsilon_checklist( $atts, $content = null ) {
	$content = str_replace('<ul>', '<ul class="checklist">', do_shortcode($content));
	return remove_wpautop($content);	
}
add_shortcode('checklist', 'epsilon_checklist');

function epsilon_bullelist( $atts, $content = null ) {
	$content = str_replace('<ul>', '<ul class="circle">', do_shortcode($content));
	return $content;
	
}
add_shortcode('bulletlist', 'epsilon_bullelist');

function epsilon_arrowlist( $atts, $content = null ) {
	$content = str_replace('<ul>', '<ul class="arrow">', do_shortcode($content));
	return $content;
	
}
add_shortcode('arrowlist', 'epsilon_arrowlist');


/* Messages Box */

function epsilon_warningbox( $atts, $content = null ) {
   return '<div class="warning">' . do_shortcode($content) . '</div>';
}
add_shortcode('warning', 'epsilon_warningbox');


function epsilon_infobox( $atts, $content = null ) {
   return '<div class="info">' . do_shortcode($content) . '</div>';
}
add_shortcode('info', 'epsilon_infobox');

function epsilon_successbox( $atts, $content = null ) {
   return '<div class="success">' . do_shortcode($content) . '</div>';
}
add_shortcode('success', 'epsilon_successbox');

function epsilon_errorbox( $atts, $content = null ) {
   return '<div class="error">' . do_shortcode($content) . '</div>';
}
add_shortcode('error', 'epsilon_errorbox');

/* Highlight */
function epsilon_highlight_yellow( $atts, $content = null ) {
   return '<span class="highlight-yellow">' . do_shortcode($content) . '</span>';
}
add_shortcode('highlight_yellow', 'epsilon_highlight_yellow');

function epsilon_highlight_dark( $atts, $content = null ) {
   return '<span class="highlight-dark">' . do_shortcode($content) . '</span>';
}
add_shortcode('highlight_dark', 'epsilon_highlight_dark');

function epsilon_highlight_green( $atts, $content = null ) {
   return '<span class="highlight-green">' . do_shortcode($content) . '</span>';
}
add_shortcode('highlight_green', 'epsilon_highlight_green');

function epsilon_highlight_red( $atts, $content = null ) {
   return '<span class="highlight-red">' . do_shortcode($content) . '</span>';
}
add_shortcode('highlight_red', 'epsilon_highlight_red');



//************************************* Pullquotes

function epsilon_pullquote_right( $atts, $content = null ) {
   return '<span class="pullquote_right">' . do_shortcode($content) . '</span>';
}
add_shortcode('pullquote_right', 'epsilon_pullquote_right');


function epsilon_pullquote_left( $atts, $content = null ) {
   return '<span class="pullquote_left">' . do_shortcode($content) . '</span>';
}
add_shortcode('pullquote_left', 'epsilon_pullquote_left');

function epsilon_italic_text( $atts, $content = null ) {
   return '<p class="italictext">' . do_shortcode($content) . '</p>';
}
add_shortcode('italic_text', 'epsilon_italic_text');


/* Dropcap */
function epsilon_drop_cap( $atts, $content = null ) {
   return '<span class="dropcap">' . do_shortcode($content) . '</span>';
}
add_shortcode('dropcap', 'epsilon_drop_cap');

/* Columns */

/* 1/2 Columns */
function epsilon_col_12( $atts, $content = null ) {
   return '<div class="col-445">' . do_shortcode($content) . '</div>';
}
add_shortcode('col_12', 'epsilon_col_12');

function epsilon_col_12_last( $atts, $content = null ) {
   return '<div class="col-445-last">' . do_shortcode($content) . '</div>';
}
add_shortcode('col_12_last', 'epsilon_col_12_last');

/* 1/3 Columns */
function epsilon_col_13( $atts, $content = null ) {
   return '<div class="col-286">' . do_shortcode($content) . '</div>';
}
add_shortcode('col_13', 'epsilon_col_13');

function epsilon_col_13_last( $atts, $content = null ) {
   return '<div class="col-286-last">' . do_shortcode($content) . '</div>';
}
add_shortcode('col_13_last', 'epsilon_col_13_last');

function epsilon_col_23( $atts, $content = null ) {
   return '<div class="col-604">' . do_shortcode($content) . '</div>';
}
add_shortcode('col_23', 'epsilon_col_23');

function epsilon_col_23_last( $atts, $content = null ) {
   return '<div class="col-604-last">' . do_shortcode($content) . '</div>';
}
add_shortcode('col_23_last', 'epsilon_col_23_last');


/* 1/4 Columns */
function epsilon_col_14( $atts, $content = null ) {
   return '<div class="col-207">' . do_shortcode($content) . '</div>';
}
add_shortcode('col_14', 'epsilon_col_14');

function epsilon_col_14_last( $atts, $content = null ) {
   return '<div class="col-207-last">' . do_shortcode($content) . '</div>';
}
add_shortcode('col_14_last', 'epsilon_col_14_last');

/* Content Columns */
function epsilon_col_173( $atts, $content = null ) {
   return '<div class="col-173 equal">' . do_shortcode($content) . '</div>';
}
add_shortcode('col_173', 'epsilon_col_173');

/* Vertical Divider */
function epsilon_vertical_separator( $atts, $content = null ) {
   return '<div class="vertical-separator equal"></div>';

}
add_shortcode('vertical_divider', 'epsilon_vertical_separator');

function epsilon_divider( $atts, $content = null ) {
   return '<hr class="content-line" />';

}
add_shortcode('divider', 'epsilon_divider');

/* Full Column */
function epsilon_fullwidthpage($atts, $content = null ) {
   return '<div class="page">' . do_shortcode($content) . '</div>';
}
add_shortcode('fullwidthpage', 'epsilon_fullwidthpage');

/* Button */

function epsilon_button_red( $atts, $content = null ) {
    extract(shortcode_atts(array(
        'link'      => '#',
    ), $atts));

	$out = "<a class=\"button-red\" href=\"" .$link. "\"><span>" .do_shortcode($content). "</span></a>";
    
    return $out;
}
add_shortcode('button_red', 'epsilon_button_red');

function epsilon_button_grey( $atts, $content = null ) {
    extract(shortcode_atts(array(
        'link'      => '#',
    ), $atts));

	$out = "<a class=\"button2\" href=\"" .$link. "\"><span>" .do_shortcode($content). "</span></a>";
    
    return $out;
}
add_shortcode('button_grey', 'epsilon_button_grey');

function epsilon_button_blue( $atts, $content = null ) {
    extract(shortcode_atts(array(
        'link'      => '#',
    ), $atts));

	$out = "<a class=\"button-blue\" href=\"" .$link. "\"><span>" .do_shortcode($content). "</span></a>";
    
    return $out;
}
add_shortcode('button_blue', 'epsilon_button_blue');

/* Images */
function epsilon_image_source( $atts, $content = null ) {
    extract(shortcode_atts(array(
        'image_url'      => '#',
        'class'      => 'imgleft',
    ), $atts));

	$out = "<img src=\"" .$image_url. "\" alt=\"\" class=\"".$class."\" />";
    
    return $out;
}
add_shortcode('image_source', 'epsilon_image_source');

#### Vimeo eg http://vimeo.com/5363880 id="5363880"
function vimeo_code($atts,$content = null){

	extract(shortcode_atts(array(  
		"id" 		=> '',
		"width"		=> $width, 
		"height" 	=> $height
	), $atts)); 
	 
	$data = "<object width='$width' height='$height' data='http://vimeo.com/moogaloop.swf?clip_id=$id&amp;server=vimeo.com' type='application/x-shockwave-flash'>
			<param name='allowfullscreen' value='true' />
			<param name='allowscriptaccess' value='always' />
			<param name='wmode' value='opaque'>
			<param name='movie' value='http://vimeo.com/moogaloop.swf?clip_id=$id&amp;server=vimeo.com' />
		</object>";
	return $data;
} 
add_shortcode("vimeo_video", "vimeo_code"); 

#### YouTube eg http://www.youtube.com/v/MWYi4_COZMU&hl=en&fs=1& id="MWYi4_COZMU&hl=en&fs=1&"
function youTube_code($atts,$content = null){

	extract(shortcode_atts(array(  
      "id" 		=> '',
  		"width"		=> $width, 
  		"height" 	=> $height
		 ), $atts)); 
	 
	$data = "<object width='$width' height='$height' data='http://www.youtube.com/v/$id' type='application/x-shockwave-flash'>
      <param name='allowfullscreen' value='true' />
			<param name='allowscriptaccess' value='always' />
			<param name='FlashVars' value='playerMode=embedded' />
			<param name='wmode' value='opaque'>
			<param name='movie' value='http://www.youtube.com/v/$id' />
		</object>";
	return $data;
} 
add_shortcode("youtube_video", "youTube_code");

/* Images */
function epsilon_imagealignment( $atts, $content = null ) {
    extract(shortcode_atts(array(
        'source'      => '',
        'align' => ''
    ), $atts));
  
  switch ($align) {
    case "left" :
      $class="alignleft";
    break;
    case "right" :
      $class="alignright";
    break;
    case "center" :
      $class="aligncenter";
    break;
  }
  
  $out = "<img class=\"".$class."\" src=\"" .$source. "\" alt=\"\">";
  
  return remove_wpautop($out);
}
add_shortcode('image', 'epsilon_imagealignment');

/* Tabs and Accordiaon */
function theme_shortcode_tabs($atts, $content = null, $code) {
	extract(shortcode_atts(array(
		'style' => false
	), $atts));
	
	if (!preg_match_all("/(.?)\[(tab)\b(.*?)(?:(\/))?\](?:(.+?)\[\/tab\])?(.?)/s", $content, $matches)) {
		return do_shortcode($content);
	} else {
		for($i = 0; $i < count($matches[0]); $i++) {
			$matches[3][$i] = shortcode_parse_atts($matches[3][$i]);
		}
		$output = '<ul class="'.$code.'">';
		
		for($i = 0; $i < count($matches[0]); $i++) {
			$output .= '<li><a href="#">' . $matches[3][$i]['title'] . '</a></li>';
		}
		$output .= '</ul>';
		$output .= '<div class="panes">';
		for($i = 0; $i < count($matches[0]); $i++) {
			$output .= '<div class="pane">' . do_shortcode(trim($matches[5][$i])) . '</div>';
		}
		$output .= '</div>';
		
		return '<div class="'.$code.'_container">' . $output . '</div>';
	}
}
add_shortcode('tabs', 'theme_shortcode_tabs');
add_shortcode('mini_tabs', 'theme_shortcode_tabs');

function theme_shortcode_accordions($atts, $content = null, $code) {
	extract(shortcode_atts(array(
		'style' => false
	), $atts));
	
	if (!preg_match_all("/(.?)\[(accordion)\b(.*?)(?:(\/))?\](?:(.+?)\[\/accordion\])?(.?)/s", $content, $matches)) {
		return do_shortcode($content);
	} else {
		for($i = 0; $i < count($matches[0]); $i++) {
			$matches[3][$i] = shortcode_parse_atts($matches[3][$i]);
		}
		$output = '';
		for($i = 0; $i < count($matches[0]); $i++) {
			$output .= '<div class="tab">' . $matches[3][$i]['title'] . '</div>';
			$output .= '<div class="pane">' . do_shortcode(trim($matches[5][$i])) . '</div>';
		}

		return '<div class="accordion">' . $output . '</div>';
	}
}
add_shortcode('accordions', 'theme_shortcode_accordions');

function theme_shortcode_toggle($atts, $content = null, $code) {
	extract(shortcode_atts(array(
		'title' => false
	), $atts));
	return '<div class="toggle"><div class="toggle_title"><h4>' . $title . '</h4></div><div class="toggle_content">' . do_shortcode(trim($content)) . '<div class="clear"></div></div></div>';
}
add_shortcode('toggle', 'theme_shortcode_toggle');

/* ======================================
   Google Map
   ======================================*/
function theme_shortcode_googlemap($atts, $content = null, $code) {
	extract(shortcode_atts(array(
		"width" => '600',
		"height" => '400',
		"address" => '',
		"latitude" => 0,
		"longitude" => 0,
		"zoom" => 1,
		"html" => '',
		"popup" => 'false',
		"controls" => '[]',
		"scrollwheel" => 'true',
		"maptype" => 'G_NORMAL_MAP',
		"marker" => 'true',
	), $atts));
	
	if($width && is_numeric($width)){
		$width = 'width:'.$width.'px;';
	}else{
		$width = '';
	}
	if($height && is_numeric($height)){
		$height = 'height:'.$height.'px';
	}else{
		$height = '';
	}
	
	$id = rand(100,1000);
	if($marker != 'false'){
		return <<<HTML
[raw]
<div id="google_map_{$id}" class="google_map" style="{$width}{$height}"></div>
<script type="text/javascript">
jQuery(document).ready(function($) {
	jQuery("#google_map_{$id}").gMap({
	    zoom: {$zoom},
	    markers:[{
	    address: "{$address}",
			latitude: {$latitude},
	    longitude: {$longitude},
	    html: "{$html}",
	    popup: {$popup}
		}],
		controls: {$controls},
		maptype: {$maptype},
	    scrollwheel:{$scrollwheel}
	});
});
</script>
[/raw]
HTML;
	}else{
return <<<HTML
[raw]
<div id="google_map_{$id}" class="google_map" style="{$width}{$height}"></div>
<script type="text/javascript">
jQuery(document).ready(function($) {
	jQuery("#google_map_{$id}").gMap({
    zoom: {$zoom},
    latitude: {$latitude},
    longitude: {$longitude},
    address: "{$address}",
    controls: {$controls},
    maptype: {$maptype},
    scrollwheel:{$scrollwheel}
	});
});
</script>
[/raw]
HTML;
	}
}
   
add_shortcode('gmap','theme_shortcode_googlemap');

?>