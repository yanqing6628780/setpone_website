<?php

/* Theme Functions  */
function excerpt($excerpt_length) {
  global $post;
	$content = $post->post_content;
	$words = explode(' ', $content, $excerpt_length + 1);
	if(count($words) > $excerpt_length) :
		array_pop($words);
		array_push($words, '...');
		$content = implode(' ', $words);
	endif;
  
  $content = strip_tags($content);
  
	echo $content;

}

function epsilon_excerpt($length, $ellipsis) {
	$text = get_the_content();
	$text = preg_replace('`\[(.*)]*\]`','',$text);
	$text = strip_tags($text);
	$text = substr($text, 0, $length);
	$text = substr($text, 0, strripos($text, " "));
	$text = $text.$ellipsis;
	return $text;
}

function epsilon_truncate($string, $limit, $break=".", $pad="...") {
	if(strlen($string) <= $limit) return $string;
	
	 if(false !== ($breakpoint = strpos($string, $break, $limit))) {
		if($breakpoint < strlen($string) - 1) {
			$string = substr($string, 0, $breakpoint) . $pad;
		}
	  }
	return $string; 
}

function strip_only_tags($str, $tags, $stripContent=false) {
    $content = '';
    if(!is_array($tags)) {
        $tags = (strpos($str, '>') !== false ? explode('>', str_replace('<', '', $tags)) : array($tags));
        if(end($tags) == '') array_pop($tags);
    }
    foreach($tags as $tag) {
        if ($stripContent)
             $content = '(.+</'.$tag.'(>|\s[^>]*>)|)';
         $str = preg_replace('#</?'.$tag.'(>|\s[^>]*>)'.$content.'#is', '', $str);
    }
    return $str;
}



function mytheme_comment($comment, $args, $depth) { 
  $GLOBALS['comment'] = $comment; ?>
		<li id="comment-<?php comment_ID() ?>">
		<div class="avatar"><?php echo get_avatar($comment,$size='64'); ?></div>
    <div class="comment-text" ><h5><?php comment_author_link() ?></h5>
      <?php if ($comment->comment_approved == '0') : ?>
  		<p>Your comment is awaiting moderation.</p>
  		<?php endif; ?>
  		<?php comment_text() ?>
      <div class="smalltext">
        <small>
          <span class="commdate"><?php comment_date('F jS, Y') ?></span>
          <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
        </small>
      </div>
    </div>		
		</li>  
<?php
}

function mytheme_ping($comment, $args, $depth) { ?>
		<li id="comment-<?php comment_ID() ?>">
		<div class="avatar"><?php echo get_avatar($comment,$size='64'); ?></div>
    <div class="comment-text" ><h5><?php comment_author_link() ?></h5>
      <?php if ($comment->comment_approved == '0') : ?>
  		<p>Your comment is awaiting moderation.</p>
  		<?php endif; ?>
  		<?php comment_text() ?>
      <div class="smalltext">
        <small>
          <span class="commdate"><?php comment_date('F jS, Y') ?></span>
          <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
        </small>
      </div>
    </div>		
		</li>  
<?php
}

function epsilon_add_javascripts() {  
  wp_enqueue_scripts('jquery');
  wp_enqueue_script( 'jquery.bxSlider.min', get_template_directory_uri().'/js/jquery.bxSlider.min.js', array( 'jquery' ) );
  wp_enqueue_script( 'jquery.cycle.all', get_template_directory_uri().'/js/jquery.cycle.all.js', array( 'jquery' ) ); 
  wp_enqueue_script( 'jquery.prettyPhoto', get_template_directory_uri().'/js/jquery.prettyPhoto.js', array( 'jquery' ) );
  wp_enqueue_script( 'jquery.colorbox', get_template_directory_uri().'/js/jquery.colorbox.js', array( 'jquery' ) );
  wp_enqueue_script( 'ddaccordion', get_template_directory_uri().'/js/ddaccordion.js', array( 'jquery' ) );
  wp_enqueue_script( 'jquery.equalheights', get_template_directory_uri().'/js/jquery.equalheights.js', array( 'jquery' ) );  
  wp_enqueue_script( 'jquery.corner', get_template_directory_uri().'/js/jquery.corner.js', array( 'jquery' ) );  
  wp_enqueue_script( 'jquery.tools.tabs.min', get_template_directory_uri().'/js/jquery.tools.tabs.min.js', array( 'jquery' ) );
  wp_enqueue_script( 'functions', get_template_directory_uri().'/js/functions.js', array( 'jquery' ) );
  
  wp_register_script( 'jquery.gmap-1.0.3-min', get_template_directory_uri().'/js/jquery.gmap-1.0.3-min.js', array('jquery'));
}

if (!is_admin()) {
  add_action( 'wp_print_scripts', 'epsilon_add_javascripts' ); 
}

if(get_option('epsilon_google_map_key')){
	function theme_add_gmap_script(){
		echo "\n<script type='text/javascript' src='http://maps.google.com/maps?file=api&amp;v=2&amp;key=".get_option('epsilon_google_map_key')."'></script>\n";
		wp_print_scripts( 'jquery.gmap-1.0.3-min');
	}
	add_filter('wp_head','theme_add_gmap_script');
}

function epsilon_add_stylesheet() { 
  ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/prettyPhoto.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/colorbox.css" type="text/css" media="screen" />
	<?php if (get_option('epsilon_switch_to_business') == "true") { ?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/styles/blue.css" type="text/css" media="screen" />
  <?php } ?>
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/custom_style.php" type="text/css" media="screen" />
  <?php
}

if (!is_admin()) {
  add_action('wp_head', 'epsilon_add_stylesheet');
}

/* Register Nav Menu Features For Wordpress 3.0 */
register_nav_menus( array(
	'topnav' => __( 'Main Navigation')
) );

/* Native Nagivation Pages List for Main Menu */
function epsilon_topmenu_pages() {
  global $excludeinclude_pages;
  
  $excludeinclude_pages = get_option('epsilon_excludeinclude_pages');
  if(is_array($excludeinclude_pages)) {
    $page_exclusions = implode(",",$excludeinclude_pages);
  }
?>
	<ul id="menu">
  	<li class="home"><a href="<?php echo home_url();?>">Home</a></li>
  	<?php wp_list_pages('title_li=&sort_column=menu_order&depth=3&exclude='.$page_exclusions);?>
  </ul>

<?php
}

/* Remove Default Container for Nav Menu Features */
function epsilon_nav_menu_args( $args = '' ) {
	$args['container'] = false;
	return $args;
} 
add_filter( 'wp_nav_menu_args', 'epsilon_nav_menu_args' );

function get_shortcode_name($name) {
  if (strstr(get_shortcode_regex(),$name)) {
    return true;
  }
}
        
function detect_ext($file) {
  $ext = pathinfo($file, PATHINFO_EXTENSION);
  return $ext;
}

function is_quicktime($file) {
  $quicktime_file = array("mov","3gp","mp4");
  if (in_array(pathinfo($file, PATHINFO_EXTENSION),$quicktime_file)) {
    return true;
  } else {
    return false;
  }
}

function is_flash($file) {
  if (pathinfo($file, PATHINFO_EXTENSION) == "swf") {
    return true;
  } else {
    return false;
  }
}

function is_youtube($file) {
  if (preg_match('/youtube/i',$file)) {
    return true;
  } else {
    return false;
  }
}

function is_vimeo($file) {
  if (preg_match('/vimeo/i',$file)) {
    return true;
  } else {
    return false;
  }
}

function epsilon_latestnews($blogcat,$num=5,$title) { 
  global $post;
  
  echo $title;
  if(is_array($blogcat)) {
    $blog_includes = implode(",",$blogcat);
  } else {
    $blog_includes = $blogcat;
  }             
  ?>
  <ul>
    <?php
      
      query_posts('cat='.$blog_includes.'&showposts='.$num);
      while ( have_posts() ) : the_post();
      $image_thumbnail = get_post_meta($post->ID, '_image_thumbnail', true );
    ?>   
      <li><a href="<?php the_permalink();?>"><?php the_title();?></a></li>
    <?php endwhile;wp_reset_query();?>            
  </ul>  
  <?php
}

function epsilon_latestworks($portocat,$num=4,$title) { 
  global $post;
  
  echo $title;
  $porto_desc = get_option('epsilon_porto_desc');

  if(is_array($portocat)) {
    $portfolio_include = implode(",",$portocat);
  } else {
    $portfolio_include =  $portocat;
  }
  $portfolio_image = get_option('epsilon_portfolio_image');
  $portfolio_video = get_option('epsilon_portfolio_video');  
  ?>
    <p><?php if ($porto_desc) echo $porto_desc;?></p>
    <ul id="latestporto"> 
      <?php
        query_posts('cat='.$portfolio_include.'&showposts='.$num);
        while ( have_posts() ) : the_post();
        $image_thumbnail = get_post_meta($post->ID, '_image_thumbnail', true );
        $portfolio_link = get_post_meta($post->ID, '_portfolio_link', true );
        $image_link = ($portfolio_link) ? $portfolio_link : $image_thumbnail;
        ?>  
        <li class=" <?php if (in_category($portfolio_video)) echo 'bgvideo'; else echo 'bgimage';?>">
        <?php if ($image_thumbnail)  : ?>
          <a href="<?php echo $image_link;?>" rel="prettyPhoto[portfolio]" title="<?php the_title();?>">			
          <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo $image_thumbnail;?>&amp;h=61&amp;w=80&amp;zc=1" alt="<?php the_title(); ?>" class="imgportfolio"/></a>
        <?php endif;?>  
        </li>
      <?php endwhile;wp_reset_query();?>    
    </ul>
  <?php     
}

function epsilon_testimonial($cat,$num=3,$title="<h2>Testimonials</h2>") {
  global $post;
  
    echo $title;
  
    query_posts('cat='.$cat.'&showposts='.$num);
    ?>
    <div id="testimonial">
    <div class="cycle-testimonial">
    <?php
    while ( have_posts() ) : the_post();
    $image_thumbnail = get_post_meta($post->ID, '_image_thumbnail', true );
    ?>
    <!-- begin of cycle-testimonial -->
      <div class="quote">
          <blockquote>
          <div><?php excerpt(20);?></div>
          </blockquote>
          <p><strong><?php the_title();?></strong></p>
          <!-- begin of nav-testimonial -->
          
          <div class="nav-testimonial">
          <a class="prev">prev</a><a class="next">next</a>
          </div>
          <!-- end of nav-testimonial -->
      </div>
    <!-- end of cycle-testimonial -->
    <?php endwhile; ?>
    <div class="clear"></div>
    </div>
    </div>    
  <?php
}

function epsilon_clientlist($cat,$num=4,$title="") {
  global $post;
  
  echo $title;

  ?>
  
	<ul class="nolistblock">  
  
  <?php
  query_posts('cat='.$cat.'&showposts='.$num);
    $counter = 0;
    while ( have_posts() ) : the_post();
    $counter++;
    ?>
		<li <?php if ($counter % 4 == 0) echo 'class="marginoff"';?>>
      <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
        <a href="<?php the_permalink();?>"><img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo thumb_url();?>&amp;h=85&amp;w=95&amp;zc=1" alt="<?php the_title(); ?>"/></a>                
      <?php } ?>
      </li>
    <?php endwhile;?>    
    </ul>	
  <?php
}

/* Get vimeo Video ID */
function vimeo_videoID($url) {
	if ( 'http://' == substr( $url, 0, 7 ) ) {
		preg_match( '#http://(www.vimeo|vimeo)\.com(/|/clip:)(\d+)(.*?)#i', $url, $matches );
		if ( empty($matches) || empty($matches[3]) ) return __('Unable to parse URL', 'epsilon');

		$videoid = $matches[3];
		return $videoid;
	}
}

function youtube_videoID($url) {
	preg_match( '#http://(www.youtube|youtube|[A-Za-z]{2}.youtube)\.com/(watch\?v=|w/\?v=|\?v=)([\w-]+)(.*?)#i', $url, $matches );
	if ( empty($matches) || empty($matches[3]) ) return __('Unable to parse URL', 'epsilon');
  
  $videoid = $matches[3];
	return $videoid;
}

function switch_image_heading() {
  $epsilon_style = get_option('epsilon_switch_to_business');
  
  if ($epsilon_style == "true") {
    echo '<img src="'.get_template_directory_uri().'/images/globe.png" alt="" class="imgtitle" />'; 
  } else {
    echo '<img src="'.get_template_directory_uri().'/images/server.png" alt="" class="imgtitle" />';
  }
}

// Make theme available for translation
// Translations can be filed in the /languages/ directory
load_theme_textdomain( 'epsilon', TEMPLATEPATH . '/languages' );

$locale = get_locale();
$locale_file = TEMPLATEPATH . "/languages/$locale.php";
if ( is_readable( $locale_file ) )
	require_once( $locale_file );


/* Enable Post Thumbnail Feature */
if (function_exists('add_theme_support')) {
	add_theme_support( 'post-thumbnails');
	set_post_thumbnail_size( 200, 200 );
	add_image_size('post_thumb', 800, 800, true);
}

function thumb_url(){  
  global $post;
  
  $thumb_src= wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array( 2100,2100 ));
  return $thumb_src[0];
}

add_theme_support('automatic-feed-links');

/* Remove Wordpress automatic formatting */
function remove_wpautop( $content ) { 
    $content = do_shortcode( shortcode_unautop( $content ) ); 
    $content = preg_replace( '#^<\/p>|^<br \/>|<p>$#', '', $content );
    return $content;
}

/**
 * Disable Automatic Formatting on Posts
 * Thanks to TheBinaryPenguin (http://wordpress.org/support/topic/plugin-remove-wpautop-wptexturize-with-a-shortcode)
 */
function theme_formatter($content) {
	$new_content = '';
	$pattern_full = '{(\[raw\].*?\[/raw\])}is';
	$pattern_contents = '{\[raw\](.*?)\[/raw\]}is';
	$pieces = preg_split($pattern_full, $content, -1, PREG_SPLIT_DELIM_CAPTURE);
	
	foreach ($pieces as $piece) {
		if (preg_match($pattern_contents, $piece, $matches)) {
			$new_content .= $matches[1];
		} else {
			$new_content .= wptexturize(wpautop($piece));
		}
	}

	return $new_content;
}
remove_filter('the_content',	'wpautop');
remove_filter('the_content',	'wptexturize');

add_filter('the_content', 'theme_formatter', 99);
?>