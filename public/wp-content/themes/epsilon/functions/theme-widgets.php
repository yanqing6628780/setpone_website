<?php

/* Widgetable Functions  */

if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'ID' => 'sidebar',
    'name'=>'Homepage_Sidebar',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h2>',
    'after_title' => '</h2>'
  ));  
  register_sidebar(array(
    'ID' => 'sidebar',
    'name'=>'General_Sidebar',
    'before_widget' => '<li id="%1$s" class="%2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h2>',
    'after_title' => '</h2>'
  ));
  register_sidebar(array(
    'ID' => 'sidebar',
    'name'=>'About_Sidebar',
    'before_widget' => '<li id="%1$s" class="%2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h2>',
    'after_title' => '</h2>'
  ));
  register_sidebar(array(
    'ID' => 'sidebar',
    'name'=>'Contact_Sidebar',
    'before_widget' => '<li id="%1$s" class="%2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h2>',
    'after_title' => '</h2>'
  ));
  register_sidebar(array(
    'ID' => 'sidebar',
    'name'=>'Blog_Sidebar',
    'before_widget' => '<li id="%1$s" class="%2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h2>',
    'after_title' => '</h2>'
  ));
  register_sidebar(array(
    'ID' => 'bottom',
    'name'=>'Bottom1',
    'before_widget' => '<div id="%1$s" class="%2$s">',
    'after_widget' => '</div>',
    'before_title' => '',
    'after_title' => ''
  ));
  register_sidebar(array(
    'ID' => 'bottom',
    'name'=>'Bottom2',
    'before_widget' => '<div id="%1$s" class="%2$s">',
    'after_widget' => '</div>',
    'before_title' => '',
    'after_title' => ''
  ));
  register_sidebar(array(
    'ID' => 'bottom',
    'name'=>'Bottom3',
    'before_widget' => '<div id="%1$s" class="%2$s">',
    'after_widget' => '</div>',
    'before_title' => '',
    'after_title' => ''
  ));      
  

/* More About Us Widget */

class PageBox_Widget extends WP_Widget {
  function PageBox_Widget() {
    $widgets_opt = array('description'=>'Display pages as small box in sidebar');
    parent::WP_Widget(false,$name= "epsilon - Page to Box",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $pageid = esc_attr($instance['pageid']);
    $opt_thumbnail = esc_attr($instance['opt_thumbnail']);
    $pageexcerpt = esc_attr($instance['pageexcerpt']);
    
		$pages = get_pages();
		$listpages = array();
		foreach ($pages as $pagelist ) {
		   $listpages[$pagelist->ID] = $pagelist->post_title;
		}
  ?>  
	 <p><label>Please select the page
		<select  name="<?php echo $this->get_field_name('pageid'); ?>">  id="<?php echo $this->get_field_id('pageid'); ?>" >
			<?php foreach ($listpages as $opt => $val) { ?>
		<option value="<?php echo $opt ;?>" <?php if ( $pageid  == $opt) { echo ' selected="selected" '; }?>><?php echo $val; ?></option>
		<?php } ?>
		</select>
		</label></p>
  <p>
		<input class="checkbox" type="checkbox" <?php if ($opt_thumbnail == "on") echo "checked";?> id="<?php echo $this->get_field_id('opt_thumbnail'); ?>" name="<?php echo $this->get_field_name('opt_thumbnail'); ?>" />
		<label for="<?php echo $this->get_field_id('opt_thumbnail'); ?>"><small>display thumbnail?</small></label><br />
    </p>
    <p><label for="pageexcerpt">Number of words for excerpt :
  		<input id="<?php echo $this->get_field_id('pageexcerpt'); ?>" name="<?php echo $this->get_field_name('pageexcerpt'); ?>" type="text" class="widefat" value="<?php echo $pageexcerpt;?>" /></label></p>  
    <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    $pageid = apply_filters('pageid',$instance['pageid']);
    $opt_thumbnail = apply_filters('opt_thumbnail',$instance['opt_thumbnail']);
    $pageexcerpt = apply_filters('pageexcerpt',$instance['pageexcerpt']);
    if ($pageexcerpt =="") $pageexcerpt = 10;
  
    $pagelist = new WP_Query('post_type=page&page_id='.$pageid);
    
    while ($pagelist->have_posts()) : $pagelist->the_post();
    $image_thumbnail = get_post_meta($post->ID, '_page_thumbnail_image', true );
    ?>
      <h2><?php the_title();?></h2>
      <?php if ($opt_thumbnail == "on") {?>
      <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
        <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo thumb_url();?>&amp;h=106&amp;w=258&amp;zc=1" alt="" class="imgbox aligncenter"/>                
      <?php }  ?>
      <?php }?>
    <p><?php excerpt($pageexcerpt);?><a href="<?php the_permalink();?>"  class="linkreadmore"> Read more &raquo;</a></p>
    <?php      
    endwhile;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("PageBox_Widget");'));

/* Latest News Widget */

class LatestNews_Widget extends WP_Widget {
  
  function LatestNews_Widget() {
    $widgets_opt = array('description'=>'epsilon Latest News Theme Widget');
    parent::WP_Widget(false,$name= "epsilon - Latest News",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $catid = esc_attr($instance['catid']);
    $newstitle = esc_attr($instance['newstitle']);
    $numnews = esc_attr($instance['numnews']);
    
    $categories_list = get_categories('hide_empty=0');
    
    $categories = array();
    foreach ($categories_list as $catlist) {
    	$categories[$catlist->cat_ID] = $catlist->cat_name;
    }

  ?>
    <p><label for="newstitle">Title:
  		<input id="<?php echo $this->get_field_id('newstitle'); ?>" name="<?php echo $this->get_field_name('newstitle'); ?>" type="text" class="widefat" value="<?php echo $newstitle;?>" /></label></p>  
	 <p><small>Please select category for <b>News</b>.</small></p>
		<select  name="<?php echo $this->get_field_name('catid'); ?>">  id="<?php echo $this->get_field_id('catid'); ?>" >
			<?php foreach ($categories as $opt => $val) { ?>
		<option value="<?php echo $opt ;?>" <?php if ( $catid  == $opt) { echo ' selected="selected" '; }?>><?php echo $val; ?></option>
		<?php } ?>
		</select>
		</label></p>	
    <p><label for="numnews">Number to display:
  		<input id="<?php echo $this->get_field_id('numnews'); ?>" name="<?php echo $this->get_field_name('numnews'); ?>" type="text" class="widefat" value="<?php echo $numnews;?>" /></label></p>
    <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    $catid = apply_filters('catid',$instance['catid']);
    $newstitle = apply_filters('newstitle',$instance['newstitle']);
    $numnews = apply_filters('numnews',$instance['numnews']);    
    
    if ($numnews == "") $numnews = 3;
    if ($newstitle == "") $newstitle = "Latest News";
    
    echo $before_widget;
    $title = $before_title.$newstitle.$after_title;
    epsilon_latestnews($catid,$numnews,$title);
    ?>
   <div class="clear"></div>
   <?php
   wp_reset_query();    
   echo $after_widget;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("LatestNews_Widget");'));

/* Client List Widget */

class clientList_Widget extends WP_Widget {
  function clientList_Widget() {
    $widgets_opt = array('description'=>'epsilon client list Theme Widget');
    parent::WP_Widget(false,$name= "epsilon - Client List",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $catid = esc_attr($instance['catid']);
    $clienttitle = esc_attr($instance['clienttitle']);
    $numclient = esc_attr($instance['numclient']);
    
    $categories_list = get_categories('hide_empty=0');
    
    $categories = array();
    foreach ($categories_list as $catlist) {
    	$categories[$catlist->cat_ID] = $catlist->cat_name;
    }

  ?>
    <p><label for="clienttitle">Title:
  		<input id="<?php echo $this->get_field_id('clienttitle'); ?>" name="<?php echo $this->get_field_name('clienttitle'); ?>" type="text" class="widefat" value="<?php echo $clienttitle;?>" /></label></p>  
	 <p><small>Please select category for <b>Testimonial</b>.</small></p>
		<select  name="<?php echo $this->get_field_name('catid'); ?>">  id="<?php echo $this->get_field_id('catid'); ?>" >
			<?php foreach ($categories as $opt => $val) { ?>
		<option value="<?php echo $opt ;?>" <?php if ( $catid  == $opt) { echo ' selected="selected" '; }?>><?php echo $val; ?></option>
		<?php } ?>
		</select>
		</label></p>	
    <p><label for="numclient">Number to display:
  		<input id="<?php echo $this->get_field_id('numclient'); ?>" name="<?php echo $this->get_field_name('numclient'); ?>" type="text" class="widefat" value="<?php echo $numclient;?>" /></label></p>
    <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    $catid = apply_filters('catid',$instance['catid']);
    $clienttitle = apply_filters('clienttitle',$instance['clienttitle']);
    $numclient = apply_filters('numclient',$instance['numclient']);    
    
    if ($numclient == "") $numclient = 4;
    if ($clienttitle == "") $clienttitle = "Our Clients";
    
    echo $before_widget;
    
    $title = $before_title.$clienttitle.$after_title;
    epsilon_clientlist($catid,$numclient,$title);
    
    echo $after_widget;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("clientList_Widget");'));

/* Testimonial Widget */

class Testimonial_Widget extends WP_Widget {
  function Testimonial_Widget() {
    $widgets_opt = array('description'=>'epsilon Testimonial Theme Widget');
    parent::WP_Widget(false,$name= "epsilon - Testimonial",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $catid = esc_attr($instance['catid']);
    $testititle = esc_attr($instance['testititle']);
    $numtesti = esc_attr($instance['numtesti']);
    
    $categories_list = get_categories('hide_empty=0');
    
    $categories = array();
    foreach ($categories_list as $catlist) {
    	$categories[$catlist->cat_ID] = $catlist->cat_name;
    }

  ?>
    <p><label for="testititle">Title:
  		<input id="<?php echo $this->get_field_id('testititle'); ?>" name="<?php echo $this->get_field_name('testititle'); ?>" type="text" class="widefat" value="<?php echo $testititle;?>" /></label></p>  
	 <p><small>Please select category for <b>Testimonial</b>.</small></p>
		<select  name="<?php echo $this->get_field_name('catid'); ?>">  id="<?php echo $this->get_field_id('catid'); ?>" >
			<?php foreach ($categories as $opt => $val) { ?>
		<option value="<?php echo $opt ;?>" <?php if ( $catid  == $opt) { echo ' selected="selected" '; }?>><?php echo $val; ?></option>
		<?php } ?>
		</select>
		</label></p>	
    <p><label for="numtesti">Number to display:
  		<input id="<?php echo $this->get_field_id('numtesti'); ?>" name="<?php echo $this->get_field_name('numtesti'); ?>" type="text" class="widefat" value="<?php echo $numtesti;?>" /></label></p>
    <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    $catid = apply_filters('catid',$instance['catid']);
    $testititle = apply_filters('testititle',$instance['testititle']);
    $numtesti = apply_filters('numtesti',$instance['numtesti']);    
    
    if ($numtesti == "") $numtesti = 1;
    if ($testititle == "") $testititle = "Testimonial";
    echo $before_widget;
    $title = $before_title.$testititle.$after_title;
    epsilon_testimonial($catid,$numtesti,$title);
    echo $after_widget;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("Testimonial_Widget");'));

/* Post to Homepage Box or Sidebar Box Widget */

class PostBox_Widget extends WP_Widget {
  function PostBox_Widget() {
    $widgets_opt = array('description'=>'Display Posts as small box in sidebar');
    parent::WP_Widget(false,$name= "epsilon - Post to Box",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $postid = esc_attr($instance['postid']);
    $opt_thumbnail = esc_attr($instance['opt_thumbnail']);
    $postexcerpt = esc_attr($instance['postexcerpt']);
    
		$centitaposts = get_posts('numberposts=-1')
		?>  
	<p><label>Please select post display
			<select  name="<?php echo $this->get_field_name('postid'); ?>">  id="<?php echo $this->get_field_id('postid'); ?>" >
				<?php foreach ($centitaposts as $post) { ?>
			<option value="<?php echo $post->ID;?>" <?php if ( $postid  ==  $post->ID) { echo ' selected="selected" '; }?>><?php echo  the_title(); ?></option>
			<?php } ?>
			</select>
	</label></p>
  <p>
		<input class="checkbox" type="checkbox" <?php if ($opt_thumbnail == "on") echo "checked";?> id="<?php echo $this->get_field_id('opt_thumbnail'); ?>" name="<?php echo $this->get_field_name('opt_thumbnail'); ?>" />
		<label for="<?php echo $this->get_field_id('opt_thumbnail'); ?>"><small>display thumbnail?</small></label><br />
    </p>
    <p><label for="postexcerpt">Number of words for excerpt :
  		<input id="<?php echo $this->get_field_id('postexcerpt'); ?>" name="<?php echo $this->get_field_name('postexcerpt'); ?>" type="text" class="widefat" value="<?php echo $postexcerpt;?>" /></label></p>  
    <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    $postid = apply_filters('postid',$instance['postid']);
    $opt_thumbnail = apply_filters('opt_thumbnail',$instance['opt_thumbnail']);
    $postexcerpt = apply_filters('postexcerpt',$instance['postexcerpt']);
    if ($postexcerpt =="") $postexcerpt = 10;
    
    echo $before_widget;
    $postlist = new WP_Query('p='.$postid);
    
    while ($postlist->have_posts()) : $postlist->the_post();
    $image_thumbnail = get_post_meta($post->ID, '_image_thumbnail', true );
    ?>
      <h2><?php the_title();?></h2>
      <?php if ($opt_thumbnail == "on") {?>
      <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
        <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo thumb_url();?>&amp;h=106&amp;w=258&amp;zc=1" alt="" class="imgbox aligncenter"/>                
      <?php }  ?>
      <?php }?>
    <p><?php excerpt($postexcerpt);?><a href="<?php the_permalink();?>"  class="linkreadmore"> Read more &raquo;</a></p>
    <?php   
    endwhile;
    echo $after_widget;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("PostBox_Widget");'));

/* Quick Tour Widget */
class quicktour_Widget extends WP_Widget {
  function quicktour_Widget () {
    $widgets_opt = array('description'=>'epsilon Quick Tour Box widget');
    parent::WP_Widget(false,$name= "epsilon - Quick Tour Box",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $quicktourtitle = esc_attr($instance['quicktourtitle']);         
    $quicktourdesc = esc_attr($instance['quicktourdesc']);         
    $quicktoururl = esc_attr($instance['quicktoururl']);         
  ?>
    <p><label for="quicktourtitle">Title:
  		<input id="<?php echo $this->get_field_id('quicktourtitle'); ?>" name="<?php echo $this->get_field_name('quicktourtitle'); ?>" type="text" class="widefat" value="<?php echo $quicktourtitle;?>" /></label></p>       		
    <p><label for="quicktourdesc">Description:
  		<textarea id="<?php echo $this->get_field_id('quicktourdesc'); ?>" name="<?php echo $this->get_field_name('quicktourdesc'); ?>" class="widefat"><?php echo $quicktourdesc;?></textarea></p>       		
    <p><label for="quicktoururl">Destination URL:
  		<input id="<?php echo $this->get_field_id('quicktoururl'); ?>" name="<?php echo $this->get_field_name('quicktoururl'); ?>" type="text" class="widefat" value="<?php echo $quicktoururl;?>" /></label></p>       		
    <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    $quicktourtitle = apply_filters('quicktourtitle',$instance['quicktourtitle']);
    $quicktourdesc = apply_filters('quicktourdesc',$instance['quicktourdesc']);
    $quicktoururl = apply_filters('quicktoururl',$instance['quicktoururl']);
    
    if ($quicktourtitle == "") $quicktourtitle = "Quick Tour";
      
    ?>
    <div class="box-grey">
        <span class="img-tour"><img src="<?php  echo get_template_directory_uri();?>/images/img-screen.png" alt=""/></span>
        <div class="box-grey-text">
        <h3><a href="<?php echo $quicktoururl;?>"><?php echo $quicktourtitle;?></a></h3>
        <small><?php echo stripslashes($quicktourdesc);?><a href="<?php echo $quicktoururl;?>"  class="linkreadmore"> Read more &raquo;</a></small>
        </div>
    </div>    
   <?php
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("quicktour_Widget");'));

/* Quick Tour Widget */
class socialLinks_Widget extends WP_Widget {
  function socialLinks_Widget () {
    $widgets_opt = array('description'=>'Display your social links profile ');
    parent::WP_Widget(false,$name= "epsilon - Social Links",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $socialtitle = esc_attr($instance['socialtitle']);         
  ?>
    <p><label for="socialtitle">Title:
  		<input id="<?php echo $this->get_field_id('socialtitle'); ?>" name="<?php echo $this->get_field_name('socialtitle'); ?>" type="text" class="widefat" value="<?php echo $socialtitle;?>" /></label></p>       		
    <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    
    $socialtitle = apply_filters('socialtitle',$instance['socialtitle']);
    
    if ($socialtitle == "") $socialtitle = "Get Connected";
    
    echo $before_widget;

    $soctitle = $before_title.$quicktourtitle.$after_title;  
    
    include (TEMPLATEPATH.'/social-links.php');
    
    echo $after_widget;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("socialLinks_Widget");'));

/* Contact Info Widget */
class ContactInfo_Widget extends WP_Widget {
  function ContactInfo_Widget () {
    $widgets_opt = array('description'=>'epsilon Contact Info Theme Widget');
    parent::WP_Widget(false,$name= "epsilon - Contact Info ",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $contact_title = esc_attr($instance['contact_title']);
    $contact_desc = esc_attr($instance['contact_desc']);

  ?>  		
    <p><label for="contact_map">Title:
  		<input type="text" id="<?php echo $this->get_field_id('contact_title'); ?>" name="<?php echo $this->get_field_name('contact_title'); ?>" class="widefat" value="<?php echo $contact_title;?>" /></label></p>                

	  <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);

    $contact_title = apply_filters('contact_title',$instance['contact_title']);
    
    if ($contact_title =="") $contact_title ="Contact Information";
    
    echo $before_widget;
    
    echo $before_title.$contact_title.$after_title;
    
    $info_officehours = get_option('epsilon_info_officehours');
    $info_phone = get_option('epsilon_info_phone');
    $info_fax = get_option('epsilon_info_fax');
    $info_address = get_option('epsilon_info_address');
    $info_website = get_option('epsilon_info_website');
    $info_email = get_option('epsilon_info_email');
    $info_latitude = get_option('epsilon_info_latitude');
    $info_longitude = get_option('epsilon_info_longitude');
    $replace_char= array(",");
    ?>
    <script type="text/javascript">
    jQuery(document).ready(function($) {
    	jQuery("#contactmap").gMap({
    	    zoom: 15,
    	    markers:[{
    	    	address: "",
    			  latitude: <?php echo $info_latitude;?>,
    	    	longitude: <?php echo $info_longitude;?>,
    	    	html: "<?php echo str_replace($replace_char,'<br/>',$info_address);?>",
    	    	popup: true
    		}],
    		controls: [],
    		maptype: G_NORMAL_MAP,
    	    scrollwheel:true
    	});
    });
    </script>
    <ul>
      <li>
      <strong>Office Hours</strong><br />
      <?php echo ($info_officehours)? $info_officehours : "Monday through Friday<br />
      8:00am - 6:00pm";?>
      </li>
      <li>
      <strong>Mailing Address</strong><br />
      <?php echo ($info_address) ? $info_address : "15 Kuningan 54th Street, 14th Floor,<br />
      Jakarta, DKI 10220, Indonesia";?>
      </li>
      <li>
      <strong>Phone and Fax</strong><br />
      <?php echo ($info_phone) ? $info_phone : "+123 456 5532";?> (phone)<br />
      <?php echo ($info_fax) ? $info_fax : "+123 456 5533";?> (fax)
      </li>
    </ul>    
    <?php echo $after_widget; ?>
    <li>
      <div style="width: 258px;height: 289px;" id="contactmap"></div>
     </a>
    </li>    
    <?php    
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("ContactInfo_Widget");'));

/* 125x125 Banner widget */
class Adbanner_Widget extends WP_Widget {
  function Adbanner_Widget() {
    $widgets_opt = array('description'=>'260x120px Banner Widget');
    parent::WP_Widget(false,$name= "Epsilon - Banner Widget",$widgets_opt);
  }
  
  function form($instance) {
    global $post;
    
    $bannertitle = esc_attr($instance['bannertitle']);
    $imagesource1 = esc_attr($instance['imagesource1']);
    $bannerimageurl1 = esc_attr($instance['bannerimageurl1']);
    $imagesource2 = esc_attr($instance['imagesource2']);
    $bannerimageurl2 = esc_attr($instance['bannerimageurl2']);

    ?>
    <p><label for="bannertitle">Title:
  		<input id="<?php echo $this->get_field_id('bannertitle'); ?>" name="<?php echo $this->get_field_name('bannertitle'); ?>" type="text" class="widefat" value="<?php echo $bannertitle;?>" /></label></p>  
      <p><label for="imagesource1">Banner Image Source 1:
  		<input id="<?php echo $this->get_field_id('imagesource1'); ?>" name="<?php echo $this->get_field_name('imagesource1'); ?>" type="text" class="widefat" value="<?php echo $imagesource1;?>" /></label></p>  
      <p><label for="bannerimageurl1">Banner Image Url 1:
  		<input id="<?php echo $this->get_field_id('bannerimageurl1'); ?>" name="<?php echo $this->get_field_name('bannerimageurl1'); ?>" type="text" class="widefat" value="<?php echo $bannerimageurl1;?>" /></label></p>  
      <p><label for="imagesource2">Banner Image Source 2:
  		<input id="<?php echo $this->get_field_id('imagesource2'); ?>" name="<?php echo $this->get_field_name('imagesource2'); ?>" type="text" class="widefat" value="<?php echo $imagesource2;?>" /></label></p>  
      <p><label for="bannerimageurl2">Banner Image Url 2:
  		<input id="<?php echo $this->get_field_id('bannerimageurl2'); ?>" name="<?php echo $this->get_field_name('bannerimageurl2'); ?>" type="text" class="widefat" value="<?php echo $bannerimageurl2;?>" /></label></p>  
    <?php    
  } 
  
  function update($new_instance, $old_instance) {
    return $new_instance;
  }
  
  function widget( $args, $instance ) {
    global $post;
    
    extract($args);
    
    $bannertitle = apply_filters('bannertitle',$instance['bannertitle']);
    $imagesource1 = apply_filters('imagesource1',$instance['imagesource1']);
    $bannerimageurl1 = apply_filters('bannerimageurl1',$instance['bannerimageurl1']);
    $imagesource2 = apply_filters('imagesource2',$instance['imagesource2']);
    $bannerimageurl2 = apply_filters('bannerimageurl2',$instance['bannerimageurl2']);
    
    if ($bannertitle == "") $bannertitle = __('Sponsor','epsilon');
    
    echo $before_widget;
    echo $before_title.$bannertitle.$after_title; 
    ?>
    <a href="<?php echo $bannerimageurl1;?>"><img src="<?php echo $imagesource1;?>" alt=""/></a>
    <a href="<?php echo $bannerimageurl2;?>"><img src="<?php echo $imagesource2;?>" alt=""/></a>
    
    <?php
    echo $after_widget;
  } 
}

add_action('widgets_init', create_function('', 'return register_widget("Adbanner_Widget");'));

?>