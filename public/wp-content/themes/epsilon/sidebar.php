                  <?php
                    $aboutpage = get_page_by_title(get_option('epsilon_about_page'));
                    $blogpage = get_option('epsilon_blog_page'); $blogpid  = get_page_by_title($blogpage);
                    $contactpage = get_option('epsilon_contact_pid');  $contactpid  = get_page_by_title($contactpage);
                  ?>
                  <!-- BEGIN OF SIDEBAR -->
                	<div class="col-300">
                    	<div class="sideright">
                        <ul>
                        	<li>
                              <?php
                              if($post->post_parent) {
                                $children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0&depth=1");
                              }else{
                                $children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0&depth=1");
                              } 
                              ?>            	
                              <?php if ($children) { ?>
                            	 <ul>
                                 <?php echo $children;?>
                               </ul>
                              <?php 
                                }
                              ?>           
                            </li>
                            <?php if (is_page($aboutpage->ID)) { ?>
                              <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('About_Sidebar')) : ?>
                                <?php 
                                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('General_Sidebar')) :
                                  get_template_part('default-widgets','widget placeholder');
                                endif; 
                                ?>
                                <?php endif;?>
                              <?php } else if (is_page($blogpid->ID) || is_category() || is_single() || is_search() || is_404()) { ?>
                              <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog_Sidebar')) : ?>
                                <?php 
                                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('General_Sidebar')) :
                                  get_template_part('default-widgets','widget placeholder');
                                endif; 
                                ?>
                                <?php endif;?>
                              <?php  } else if (is_page($contactpid->ID)) { ?>
                              <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Contact_Sidebar')) : ?>
                                <?php 
                                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('General_Sidebar')) :
                                  get_template_part('default-widgets','widget placeholder');
                                endif; 
                                ?>
                                <?php endif;?>
                              <?php } else { ?>
                              <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('General_Sidebar')) : ?>
                                <?php get_template_part('default-widgets','widget placeholder');?>
                              <?php endif;?>
                            <?php } ?>
                          </ul>
                        </div>
                    </div>
                    <!-- END OF SIDEBAR -->