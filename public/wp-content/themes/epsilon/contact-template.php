<?php
/*
Template Name: Contact Form
*/
?>
<?php get_header();?>

            <!-- BEGIN OF PAGE TITLE -->
            <div id="header-inner">
                <h1><?php the_title();?></h1>
                <?php 
                global $post;
                $page_heading_icon = get_post_meta($post->ID, '_page_heading_icon', true );
                if ($page_heading_icon !="") { ?>
                  <img src="<?php echo $page_heading_icon;?>"  alt="" class="imgtitle" />
                <?php } else {
                  switch_image_heading();
                }
                ?>
            </div>
            <!-- END OF PAGE TITLE -->
            
            <!-- BEGIN OF CONTENT -->
            <div id="content-inner-sidebar">
                	 <!-- begin of col-620  -->
                	<div class="col-620">
                    	<div id="maintext">
                    	 <div id="boxcontact-col">
                      	<?php if (have_posts()) : while (have_posts()) : the_post();?>
                      	
                      	 <?php the_content();?>
                      	
                      	<?php endwhile;endif;?>
                      	<?php wp_reset_query();?>
                        </div>
                        <div class="clear"></div>
                       <!-- begin of contact-area -->
                       <div class="contact-area">

                            <?php $success_msg  = get_option('epsilon_success_msg');?>
                            <div class="success-contact"><?php echo ($success_msg) ? $success_msg : "Your message has been sent successfully. Thank you!";?></div>                                
                           <div id="contactFormArea">                                  
                           <form action="#" id="contactform"> 
                              <fieldset>
                                    <label><?php echo __('Name','epsilon');?></label>
                                    <input type="text" name="name" class="textfield" id="name" value="" />
                                    <label><?php echo __('Email','epsilon');?></label>
                                    <input type="text" name="email" class="textfield" id="email" value="" />
                                    <label><?php echo __('Subject','epsilon');?></label>
                                    <input type="text" name="subject" class="textfield" id="subject" value="" />
                                    <div class="contact-column-right">
                                        <label><?php echo __('Message','epsilon');?></label>
                                        <textarea name="message" id="message" class="textarea" cols="50" rows="10"></textarea>
                                        <label>&nbsp;</label>
                                        <button type="submit" name="submit" id="buttonsend" class="submit buttoncontact"><?php echo __('Send now','epsilon');?></button>
                                        <input type="hidden" name="siteurl" id="siteurl" value="<?php echo get_template_directory_uri();?>" />
                                        <span class="loading" style="display: none;"><label><?php echo __('Please wait...','epsilon');?></label></span>
                                    </div>
                              </fieldset> 
                              </form>    
                            </div>
                        </div>
                         <!-- end of contact-area -->
                                                 
                      </div><!-- end of maintext -->
                    </div>
                    <!-- end of col-620 -->
                    <?php get_sidebar();?>
                    
                    <div class="clear"></div>
            </div>
            <div id="content-inner-sidebar-bottom"></div>
            <!-- END OF CONTENT -->
        </div>
        <!-- end of main -->
        
<?php get_footer();?>