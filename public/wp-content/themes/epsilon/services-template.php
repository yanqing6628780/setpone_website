<?php
/*
Template Name: Services Template
*/
?>

<?php get_header();?>

            <!-- BEGIN OF PAGE TITLE -->
            <div id="header-inner">
                <h1><?php the_title();?></h1>
                <?php 
                global $post;
                $page_heading_icon = get_post_meta($post->ID, '_page_heading_icon', true );
                if ($page_heading_icon !="") { ?>
                  <img src="<?php echo $page_heading_icon;?>"  alt="" class="imgtitle" />
                <?php } else {
                  switch_image_heading();
                }
                ?>
            </div>
            <!-- END OF PAGE TITLE -->
            
            <!-- BEGIN OF CONTENT -->
            <div id="content-inner">
                <!-- begin of col-920 (services) -->
                <div class="col-920 fullwidth-serv">
                  <?php
                    global $post;
                   	$services_page = get_option('epsilon_services_pid');
                   	$servicespage = get_page_by_title($services_page);
                   	$servicespid = ($post->ID) ? $post->ID: $servicespage->ID;
                    $services_order = get_option('epsilon_services_order') ? get_option('epsilon_services_order') : "date";
                   ?>                            	
                  <?php
                    query_posts('page_id='.$servicespid);
              	     while (have_posts()) : the_post();
              	     the_content();
              	     endwhile;
                    ?>
                	<!-- begin of col-900 (services) -->
                    <div class="col-900">
                      <?php
                        query_posts('post_type=page&showposts=-1&post_parent='.$servicespid.'&orderby='.$services_order);
                        $counter = 0; 
                        while ( have_posts() ) : the_post();
                        $services_thumbnail = get_post_meta($post->ID,'_page_thumbnail_image',true);
                        $subtitle  = get_post_meta($post->ID,'_subtitle',true);
                        $counter++;
                      ?>
                    
                        <div class="col-280 boxservices">
                        	<div class="tb">
                            <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
                                <img src="<?php echo thumb_url();?>" alt="" class="imgleft"/>                
                              <?php } else { ?>
                              <?php if ($services_thumbnail !="") { ?>
                                <img src="<?php echo $services_thumbnail;?>" alt="" class="imgleft"/>
                              <?php }
                                }
                              ?>
                            <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                            <span><?php echo ($subtitle) ? $subtitle : "Your Service Subtitle Goes Here";?></span>
                          </div>
                        	<p><?php excerpt(60);?>
                        </div>
                         <?php if ($counter % 3 !=0 ) echo '<div class="vertical-separator-noline"></div>';?>
                     <?php endwhile;?>
                    </div>
                     <!-- end of col-900 (services) -->
                </div>
                <!-- end of col-920 (services) -->
            </div>
            <!-- END OF CONTENT -->
        </div>
        <!-- end of main -->
        
<?php get_footer();?>