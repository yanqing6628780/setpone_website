                <!-- begin hosting-col (hosting type) -->
                <div id="hosting-col">
                
                    <div class="col-173 equal">
                        <span class="boxtitle-hosting">
                            <span class="price"><?php echo $currency;?><?php echo ($homebox_price1) ? stripslashes($homebox_price1) : "5";?></span>/month<br />
                            <span class="redtext"><?php echo ($homebox_title1) ? stripslashes($homebox_title1) : "Basic Hosting";?></span>
                        </span>
                        <p><img src="<?php echo ($homebox_image1) ? $homebox_image1 : get_template_directory_uri()."/images/img-hosting.gif";?>" alt="" class="imgcenter" /></p>
                        <?php echo ($homebox_desc1) ? stripslashes($homebox_desc1) : "
                        <ul class=\"circle\">
                            <li>Feature number one</li>
                            <li>Feature number two</li>
                            <li>Feature number three</li>
                        </ul>";?>
                        <div class="center"><a class="button" href="<?php echo ($homebox_desturl1) ? $homebox_desturl1 : "#";?>"><?php echo __('Sign Up','epsilon');?></a></div>
                    </div>
                    <div class="vertical-separator equal ie7line"></div>
                    <div class="col-173 equal">
                        <span class="boxtitle-hosting">
                            <span class="price"><?php echo $currency;?><?php echo ($homebox_price2) ? stripslashes($homebox_price2) : "15";?></span>/month<br />
                            <span class="redtext"><?php echo ($homebox_title2) ? stripslashes($homebox_title2) : "Medium Hosting";?></span>
                        </span>
                        <p><img src="<?php echo ($homebox_image2) ? $homebox_image2 : get_template_directory_uri()."/images/img-hosting2.gif";?>" alt="" class="imgcenter" /></p>
                        <?php echo ($homebox_desc2) ? stripslashes($homebox_desc2) : "
                        <ul class=\"circle\">
                            <li>Feature number one</li>
                            <li>Feature number two</li>
                            <li>Feature number three</li>
                        </ul>";?>                       
                        <div class="center"> <a class="button" href="<?php echo ($homebox_desturl2) ? $homebox_desturl2 : "#";?>"><?php echo __('Sign Up','epsilon');?></a></div>
                    </div>
                    <div class="vertical-separator equal ie7line"></div>
                    <div class="col-173 equal">
                        <span class="boxtitle-hosting">
                            <span class="price"><?php echo $currency;?><?php echo ($homebox_price3) ? stripslashes($homebox_price3) : "25";?></span>/month<br />
                            <span class="redtext"><?php echo ($homebox_title3) ? stripslashes($homebox_title3) : "Professional Hosting";?></span>
                        </span>
                        <p><img src="<?php echo ($homebox_image3) ? $homebox_image3 : get_template_directory_uri()."/images/img-hosting3.gif";?>" alt="" class="imgcenter" /></p>
                        <?php echo ($homebox_desc3) ? stripslashes($homebox_desc3) : "
                        <ul class=\"circle\">
                            <li>Feature number one</li>
                            <li>Feature number two</li>
                            <li>Feature number three</li>
                        </ul>";?>
                       <div class="center"><a class="button" href="<?php echo ($homebox_desturl3) ? $homebox_desturl3 : "#";?>"><?php echo __('Sign Up','epsilon');?></a></div>
                    </div>
                      <div class="vertical-separator equal ie7line"></div>
                    <div class="col-280 equal">
                      <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Homepage_Sidebar')) : ?>
                        <h3>Why choose our services</h3>
                        <p>At vero eos et accusamus et iusto odio dignisimo ducimus qui blanditiis praesentium volup tatuma deleniti atque corrupti quos dolores qua molestia  excepturi sint occaecati cupiditateno voluptasend</p>
						            <?php include (TEMPLATEPATH.'/quicktour.php');?>
                      <?php endif;?>
                  </div>
                  <div class="clear"></div>
                </div>
                <!-- end of hosting-col (hosting type) -->