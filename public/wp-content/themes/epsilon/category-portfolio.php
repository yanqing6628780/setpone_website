<?php get_header();?>

            <!-- BEGIN OF PAGE TITLE -->
            <div id="header-inner">
                <h1><?php single_cat_title();?></h1>
                <?php switch_image_heading();?>
            </div>
            <!-- END OF PAGE TITLE -->
            
            <!-- BEGIN OF CONTENT -->
            <div id="content-inner">
                <!-- begin of col-920 (services) -->
                <div class="col-920 fullwidth">        
                  <!-- begin of col-900 (services) -->
                    <div class="col-900">
                  <?php
                    while ( have_posts() ) : the_post();
                    $counter++;
                    $image_thumbnail = get_post_meta($post->ID, '_image_thumbnail', true );
                    $portfolio_link = get_post_meta($post->ID, '_portfolio_link', true );
                    $image_link = ($portfolio_link) ? $portfolio_link : $image_thumbnail;
                  ?>                
                	
                    <div class="col-280 boxpf">
                    	<a href="<?php echo $image_link;?>" rel="prettyPhoto[portfolio]" >
                        <?php
                        if ($image_thumbnail)  { ?>			
                        <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo $image_thumbnail;?>&amp;h=128&amp;w=280&amp;zc=1" alt="" />
                        <?php }?>
                        </a>
                        <h4><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
                    	<p><?php excerpt(10);?></p>
                    </div>
                     <?php if ($counter % 3 !=0 ) echo '<div class="vertical-separator-noline"></div>';?>
                     <?php endwhile;?>
                    <div class="pages pfpages">
                    <?php 
                		if (function_exists('wp_pagenavi')) :
                		    wp_pagenavi();
                		  else : 
                		?>
                  		<div class="navigation">
                        <div class="alignleft"><?php next_posts_link(__('&laquo; Previous Entries','epsilon')) ?></div>
                        <div class="alignright"><?php previous_posts_link(__('Next Entries &raquo;','epsilon')) ?></div>
                        <div class="clear"></div>
                  		</div>
                    <?php endif;?>  
                    </div>
                    </div>
                     <!-- end of col-900 (services) -->
                </div>
                <!-- end of col-920 (services) -->
            </div>
            <!-- END OF CONTENT -->
        </div>
        <!-- end of main -->
        
<?php get_footer();?>