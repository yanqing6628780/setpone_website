<?php get_header();?>
<!-- FAQ Setting -->
<script type="text/javascript">
	ddaccordion.init({
	headerclass: "ask", 
	contentclass: "question", 
	revealtype: "click", 
	mouseoverdelay: 200, 
	collapseprev: false, 
	defaultexpanded: [], 
	onemustopen: false, 
	animatedefault: false,
	persiststate: false, 
	toggleclass: ["closedquestion", "openquestion"], 
	togglehtml: ["prefix", "<img src='<?php echo get_template_directory_uri();?>/images/closed.gif' alt='' style='width:13px; height:13px; float:right;' /> ", "<img src='<?php echo get_template_directory_uri();?>/images/open.gif' alt='' style='width:13px; height:13px; float:right;' /> "], 
	animatespeed: "fast", 
	oninit:function(expandedindices){ 
	},
	onopenclose:function(header, index, state, isuseractivated){ 		
	}
})
</script>
            <!-- BEGIN OF PAGE TITLE -->
            <div id="header-inner">
                <h1><?php single_cat_title();?></h1>
                <?php
                  $blog_page = get_option('epsilon_blog_page');
                  $blog_page_id = get_page_by_title($blog_page);
                  $page_heading_icon = get_post_meta($blog_page_id->ID, '_page_heading_icon', true );
                  if ($page_heading_icon !="") { ?>
                    <img src="<?php echo $page_heading_icon;?>"  alt="" class="imgtitle" />
                  <?php } else {
                    switch_image_heading();
                  }
                ?>
            </div>
            <!-- END OF PAGE TITLE -->
            
            <!-- BEGIN OF CONTENT -->
            <div id="content-inner-sidebar">
                	 <!-- begin of col-620  -->
                	<div class="col-620">
                    	<div id="maintext">
                          <?php
                          while ( have_posts() ) : the_post();
                        	?>                                
                        
                            <div class="ask"><?php the_title();?></div>
                            <div class="question">
                            <?php the_content();?>
                            </div>
                        <?php endwhile;?>
                                      
                        </div><!-- end of maintext -->
                    </div>
                    <!-- end of col-620 -->
                    
                    <?php get_sidebar();?>
                    
                    <div class="clear"></div>
            </div>
            <div id="content-inner-sidebar-bottom"></div>
            <!-- END OF CONTENT -->
        </div>
        <!-- end of main -->
        
<?php get_footer();?>