<?php header("Content-type: text/css; charset: UTF-8"); ?>

<?php require_once( '../../../../wp-load.php' );?>

<?php
   
  $custom_css = get_option('epsilon_custom_css');
  $custom_body_text = get_option('epsilon_custom_body_text');
  
  if ($custom_body_text !== "") {
    echo 'body{
    	font-family:'.$custom_body_text['face'].';
    	font-size:'.$custom_body_text['size'].'px;
    	color:'.$custom_body_text['color'].';
    }';
  }
  
  if ($custom_css !="") {
    echo $custom_css;
  }
?>