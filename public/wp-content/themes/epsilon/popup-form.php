<!-- begin sign up and login popup -->
    <div style='display:none'>
        <div id='signup-content' style='padding:20px; background:#fff;'>
        <h2><?php bloginfo('blogname');?><?php echo __(' sign up form','epsilon');?></h2>
        <hr class="double-form" />                        
        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non</p>        
        <form action="" method="post" class="popup-form">
            <fieldset>
            <label class="popup"><?php echo __('Name','epsilon');?></label>
            <input class="input"  type="text" size="25" />
            <label class="popup"><?php echo __('Email','epsilon');?></label>
            <input class="input" type="text" size="25" />
            <label class="popup"><?php echo __('Address','epsilon');?></label>
            <textarea cols="40" rows="5" class="textarea-popup"></textarea>
            <label class="popup"><?php echo __('City','epsilon');?></label>
            <input class="input"  type="text" size="25" />
            <label class="popup"><?php echo __('Country','epsilon');?></label>
            <input class="input"  type="text" size="25" />
            <label class="popup"><?php echo __('Zip Postal','epsilon');?></label>
            <input class="input"  type="text" size="25" />            
            <button type="submit" class="submit submit-popup"><?php echo __('Submit','epsilon');?></button>            
            </fieldset>
        </form>             
        </div>
        
        <div id='login-content' style='padding:20px; background:#fff;'>
        <h2><?php bloginfo('blogname');?><?php echo __('login form','epsilon');?></h2>
        <hr class="double-form" />                        
        <form action="<?php echo home_url() ?>/wp-login.php" method="post" class="popup-form">
            <fieldset>
            <label class="popup"><?php echo __('Username','epsilon');?></label>
            <input class="input" name="log" id="log"  type="text" size="25" />
            <label class="popup"><?php echo __('Password','epsilon');?></label>
            <input class="input" type="password" size="25" name="pwd" id="pwd" />
            <input type="hidden" name="redirect_to" value="<?php echo home_url() ?>"/>            
            <button type="submit" class="submit submit-popup"><?php echo __('Submit','epsilon');?></button>            
            </fieldset>
        </form>                     
        </div>
    </div>
    <!-- end of sign up and login popup -->