<?php get_header();?>

            <!-- BEGIN OF PAGE TITLE -->
            <div id="header-inner">
                <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
                <?php /* If this is a category archive */ if (is_category()) { ?>
                <h1><?php echo __('Archive for the ','ovum');?>&#8216;<?php single_cat_title(); ?>&#8217; Category</h1>
                <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
                <h1><?php echo __('Posts Tagged ','ovum');?>&#8216;<?php single_tag_title(); ?>&#8217;</h1>
                <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
                <h1><?php echo __('Archive for ','ovum');?><?php the_time('F jS, Y'); ?></h1>
                <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
                <h1><?php echo __('Archive for ','ovum');?><?php the_time('F, Y'); ?></h1>
                <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
                <h1><?php echo __('Archive for','ovum');?> <?php the_time('Y'); ?></h1>
                <?php /* If this is an author archive */ } elseif (is_author()) { ?>
                <h1><?php echo __('Author Archive','ovum');?></h1>
                <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
                <h1><?php echo __('Blog Archives','ovum');?></h1>
                <?php } ?>         
                <?php
                  global $post;
                  $blog_page = get_option('epsilon_blog_page');
                  $blog_page_id = get_page_by_title($blog_page);
                  $page_heading_icon = get_post_meta($blog_page_id->ID, '_page_heading_icon', true );
                  if ($page_heading_icon !="") { ?>
                    <img src="<?php echo $page_heading_icon;?>"  alt="" class="imgtitle" />
                  <?php } else {
                    switch_image_heading();
                  }
                ?>
            </div>
            <!-- END OF PAGE TITLE -->
            
            <!-- BEGIN OF CONTENT -->
            <div id="content-inner-sidebar">
                	 <!-- begin of col-620  -->
                	<div class="col-620">
                    	<div id="maintext">
                          <?php
                          
                          $numtext = (get_option('epsilon_blogtext')) ? get_option('epsilon_blogtext') : 60;
                          while ( have_posts() ) : the_post();
                          $image_thumbnail = get_post_meta($post->ID, '_image_thumbnail', true );
                        	?>        
                    	
                        	 <div class="post">
                            	<h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                                <div class="metadata">
                                  <?php echo __('Posted by ','epsilon');?><?php the_author_posts_link();?> <?php echo __('on ','epsilon');?><?php the_time('F d, Y');?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php comments_popup_link(__('0 Comment','epsilon'),__('1 Comment','epsilon'),__('% Comments','epsilon'));?>
                                </div>
                                <div class="entry">
                                <?php if ($image_thumbnail)  : ?>			
                                  <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo $image_thumbnail;?>&amp;h=180&amp;w=585&amp;zc=1" alt="" />
                                <?php endif;?>    
                                <p><?php excerpt($numtext);?></p>
                                <div class="butmore"><a class="more" href="<?php the_permalink();?>"><?php echo __('Read more','epsilon');?></a></div>
                                </div>
                            </div><!-- end of post -->
                            <?php endwhile;?>
                            <div class="pages blogpages">
                              <?php 
                          		if (function_exists('wp_pagenavi')) :
                          		    wp_pagenavi();
                          		  else : 
                          		?>
                            		<div class="navigation">
                            			<div class="alignleft"><?php next_posts_link(__('&laquo; Previous Entries','epsilon')) ?></div>
                            			<div class="alignright"><?php previous_posts_link(__('Next Entries &raquo;','epsilon')) ?></div>
                            			<div class="clear"></div>
                            		</div>
                              <?php endif;?>                              
                            </div>
                        </div><!-- end of maintext -->
                    </div>
                    <!-- end of col-620 -->
                    
                    <?php get_sidebar();?>
                    
                    <div class="clear"></div>
            </div>
            <div id="content-inner-sidebar-bottom"></div>
            <!-- END OF CONTENT -->
        </div>
        <!-- end of main -->
        
<?php get_footer();?>