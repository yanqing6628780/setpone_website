                        <?php echo ($soctitle) ? "<h2>$soctitle</h2>" : "";?>
                        <ul class="socialicon">
                          <?php
                            $twitter_id = get_option('epsilon_twitter_id');
                            $facebook_id = get_option('epsilon_facebook_id');
                            $linkedin_id = get_option('epsilon_linkedin_id');
                            $youtube_id = get_option('epsilon_youtube_id');
                            $flickr_user = get_option('epsilon_flickr_user');
                          ?>             
                        	<li><img src="<?php echo get_template_directory_uri();?>/images/icon-fb.gif" alt="" />&nbsp;&nbsp;<a href="http://facebook.com/<?php echo ($facebook_id);?>"><?php echo __('Become a fan on Facebook','epsilon');?></a></li>
                          <li><img src="<?php echo get_template_directory_uri();?>/images/icon-twitter.gif" alt=""/>&nbsp;&nbsp;<a href="http://twitter.com/<?php echo ($twitter_id);?>"><?php echo __('Follow us on Twitter','epsilon');?></a></li>
                          <li><img src="<?php echo get_template_directory_uri();?>/images/icon-feed.gif" alt=""/>&nbsp;&nbsp;<a href="<?php bloginfo('rss2_url');?>"><?php echo __('Subscribe our RSS','epsilon');?></a></li>
                          <li><img src="<?php echo get_template_directory_uri();?>/images/icon-linkedln.gif" alt="" />&nbsp;&nbsp;<a href="http://id.linkedin.com/in/<?php echo ($linkedin_id);?>"><?php echo __('Find us on Linkedin','epsilon');?></a></li>
                        </ul>