<?php
/*
Template Name: Product Template 2
*/
?>
<?php get_header();?>

            <!-- BEGIN OF PAGE TITLE -->
            <div id="header-inner">
                <h1><?php the_title();?></h1>
                <?php 
                global $post;
                $page_heading_icon = get_post_meta($post->ID, '_page_heading_icon', true );
                if ($page_heading_icon !="") { ?>
                  <img src="<?php echo $page_heading_icon;?>"  alt="" class="imgtitle" />
                <?php } else {
                  switch_image_heading();
                }
                ?>
            </div>
            <!-- END OF PAGE TITLE -->
            
            <!-- BEGIN OF CONTENT -->
            <div id="content-inner">
                <!-- begin of col-920 (web-hosting) -->
                <div class="col-920 fullwidth">
                    <?php $product_desc = get_option('epsilon_product_desc');?>
                    <p><?php echo stripslashes($product_desc);?></p> 
                    
                    <div id="filter">
              			<ul>
                    	<?php  
                      $categories = get_categories('taxonomy=product_category&orderby=ID&title_li=&hide_empty=0');
                      foreach ($categories as $category) { 
                      $termlink = get_term_link($category->slug,$category->taxonomy);
                      ?>
                        <li><a  class="<?php if (get_query_var($category->taxonomy) == $category->slug) echo 'current';?>" href="<?php echo $termlink;?>"><span><?php echo $category->name;?></span></a></li>
                        <?php
                      }
                      ?>
                  	</ul>
                    </div>
                                  
                    <!-- BEGIN OF PRICING BOX -->
                    <div id="dedicated-boxe">

                      <?php
                      	$product_order = get_option('epsilon_product_order') ? get_option('epsilon_product_order') : "date";
                        $product_cat = get_option('epsilon_product_cat');
                        $billing_cycle = get_option('epsilon_billing_cycle');
                        
                        $page = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $product_num = (get_option('epsilon_product_num')) ? get_option('epsilon_product_num') : 4;
                        $product_excerpt = (get_option('epsilon_product_excerpt')) ? get_option('epsilon_product_excerpt') : 37;
                        
                        if (post_type_exists('product')) {
                          query_posts(array( 'post_type' => 'product', 'showposts' => $product_num,'orderby'=>$product_order,'product_category'=>$product_cat));
                        }
                        
                        $counter = 0; 
                        $counter_border += 1;
                        while ( have_posts() ) : the_post();
                        $counter++;
                        $counter_border++;
                        $product_price = get_post_meta($post->ID,'_product_price',true);
                        $product_image = get_post_meta($post->ID,'_product_image',true);
                        $product_url = get_post_meta($post->ID,'_product_url',true) ? get_post_meta($post->ID,'_product_url',true) : get_permalink();
                        $featured_product = get_post_meta($post->ID,'_featured_product',true);
                        $product_feature = get_post_meta($post->ID,'_product_feature',true);
                      ?>                                        
                    	<div class="row">
                          	<div class="col1">
                            <?php if (function_exists('has_post_thumbnail') && has_post_thumbnail()) {?>
                              <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo thumb_url();?>&amp;h=63&amp;w=144&amp;zc=1" alt="" class="imgcenter"/>                
                            <?php } else { ?>
                              <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo $product_image;?>&amp;h=63&amp;w=144&amp;zc=1" alt="" class="imgcenter" /></p>
                            <?php } ?>
                            </div>
                            <div class="col2">
                                <h3><?php the_title();?></h3>
                                <p><?php excerpt(10);?></p>
                                <div class="bgwhite">
                                <?php echo $product_feature;?> 
                                </div>
                            </div>
                            <div class="col3">
                            <?php if ($featured_product == "yes") echo '<span class="best"><img src="'.get_template_directory_uri().'/images/bestseller.png" alt="" /></span>';?>
                        	<div style="text-align:center">
                            <br />
                            <span class="boxtitle-hosting"><br />
                                <span class="price"><?php echo $currency;?><?php echo ($product_price);?></span>/<?php if ($billing_cycle == "monthly") echo 'month'; else echo 'year';?><br />
                                <span class="redtext"><?php echo __('Value choices','epsilon');?></span>
                            </span>
                            </div>
                            <div class="center"> <a class="button-alt" href="<?php echo $product_url;?>"><?php echo __('Sign Up','epsilon');?></a></div>
                            </div>
                            <div class="clear"></div>
                        </div><!-- end of row -->
                      <?php endwhile;?>
                        
                    </div>
                    <!-- END OF PRICING BOX -->  
                    
                    <div id="webhosting-col">
                	 <!-- begin of col-440 (guarantee) -->
                	<div class="col-440 equal">
                	   <?php
                	   $product_addinfo_title_box1 = get_option('epsilon_product_addinfo_title_box1');
                	   $product_addinfo_desc_box1  = get_option('epsilon_product_addinfo_desc_box1');
                	   $product_addinfo_title_box2 = get_option('epsilon_product_addinfo_title_box2');
                	   $product_addinfo_desc_box2  = get_option('epsilon_product_addinfo_desc_box2');                	   
                	   ?>
                    	<h3><?php echo ($product_addinfo_title_box1) ? $product_addinfo_title_box1 : "99% Uptime guarantee";?></h3>
                        <p><img src="<?php echo get_template_directory_uri();?>/images/guarantie.gif" alt="" class="imgleft" />
                        <?php echo ($product_addinfo_desc_box1) ? $product_addinfo_desc_box1 : "Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet voluptates repudiandae sinteta molestiae non recusandae. Itaque earum rerum hic teneturus  sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellates.";?></p>
                    </div>
                    <!-- end of col-440 (guarantee) -->
                    <div class="vertical-separator equal"></div>
                    <!-- begin of col-440 (We accept) -->
                	 <div class="col-440 equal">
                    	<h3><?php echo ($product_addinfo_title_box2) ? stripslashes($product_addinfo_title_box2) : "We accept";?></h3>
                    	 <?php if ($product_addinfo_desc_box2) :
                          echo stripslashes($product_addinfo_desc_box2); 
                        else : ?>
                        <span>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus</span>
                        <?php endif;?>
                        <ul class="nolistblock" style="padding-top:10px">
                        	<li><img src="<?php echo get_template_directory_uri();?>/images/paypal.gif" alt="" /></li>
                            <li><img src="<?php echo get_template_directory_uri();?>/images/visa.gif" alt="" /></li>
                            <li><img src="<?php echo get_template_directory_uri();?>/images/master-card.gif" alt="" /></li>
                            <li><img src="<?php echo get_template_directory_uri();?>/images/ae.gif" alt="" /></li>
                            <li class="marginoff"><img src="<?php echo get_template_directory_uri();?>/images/o.gif" alt="" /></li>
                        </ul>                        
                    </div>
                    <!-- end of col-440 (We accept) -->
                    </div>
                </div>
                <!-- end of col-920 (web-hosting) -->
            </div>
            <!-- END OF CONTENT -->
        </div>
        <!-- end of main -->
        
<?php get_footer();?>