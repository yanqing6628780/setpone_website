<?php get_header();?>

            <!-- BEGIN OF PAGE TITLE -->
            <div id="header-inner">
                <h1><?php echo __('Search Results for ','epsilon');?> <?php echo '"'.$s.'"';?></h1>
                <?php switch_image_heading();?>
            </div>
            <!-- END OF PAGE TITLE -->
            
            <!-- BEGIN OF CONTENT -->
            <div id="content-inner-sidebar">
                	 <!-- begin of col-620  -->
                	<div class="col-620">
                    	<div id="maintext">
                          <?php
                          
                          $numtext = (get_option('epsilon_blogtext')) ? get_option('epsilon_blogtext') : 60;
                          if (have_posts()) : 
                          while ( have_posts() ) : the_post();
                          $image_thumbnail = get_post_meta($post->ID, '_image_thumbnail', true );
                        	?>        
                    	
                        	 <div class="post">
                            	<h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                                <div class="metadata">
                                  <?php echo __('Posted by ','epsilon');?><?php the_author_posts_link();?> <?php echo __('on ','epsilon');?><?php the_time('F d, Y');?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php comments_popup_link(__('0 Comment','epsilon'),__('1 Comment','epsilon'),__('% Comments','epsilon'));?>
                                </div>
                                <div class="entry">
                                <?php if ($image_thumbnail)  : ?>			
                                  <img src="<?php echo get_template_directory_uri();?>/timthumb.php?src=<?php echo $image_thumbnail;?>&amp;h=180&amp;w=585&amp;zc=1" alt="" />
                                <?php endif;?>    
                                <p><?php excerpt($numtext);?></p>
                                <div class="butmore"><a class="more" href="<?php the_permalink();?>"><?php echo __('Read more','epsilon');?></a></div>
                                </div>
                            </div><!-- end of post -->
                            <?php endwhile;?>
                            <?php else : ?>
                              <h2><?php echo __('No Posts Found for "'.$s.'"!','epsilon');?></h2>
                              <h4><?php echo __('Try Different Search?','epsilon');?></h4>
                              <?php get_search_form();?>                                                        
                            <?php endif;?>  
                            <div class="pages blogpages">
                              <?php 
                          		if (function_exists('wp_pagenavi')) :
                          		    wp_pagenavi();
                          		  else : 
                          		?>
                            		<div class="navigation">
                            			<div class="alignleft"><?php next_posts_link(__('&laquo; Previous Entries','epsilon')) ?></div>
                            			<div class="alignright"><?php previous_posts_link(__('Next Entries &raquo;','epsilon')) ?></div>
                            			<div class="clear"></div>
                            		</div>
                              <?php endif;?>                              
                            </div>
                        </div><!-- end of maintext -->
                    </div>
                    <!-- end of col-620 -->
                    
                    <?php get_sidebar();?>
                    
                    <div class="clear"></div>
            </div>
            <div id="content-inner-sidebar-bottom"></div>
            <!-- END OF CONTENT -->
        </div>
        <!-- end of main -->
        
<?php get_footer();?>