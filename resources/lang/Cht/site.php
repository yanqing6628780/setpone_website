<?php

return [
    'home' => '<a href="/Cht">首頁</a>',
    'about' => '<a href="/Cht/about">關於我們</a>',
    'contact' => '<a href="/Cht/contact">聯繫我們</a>',
    'breadCrumbContentTitle' => '正文',
    'lang' => 'Language',
    'news_title' => '新聞',
    'service_title' => '服務',
    'about_title' => '關於我們',
    'contact_title' => '聯繫我們',
    'partener_title' => '合作夥伴',
    'contact_info' => 'STEP ONE Solutions Limited <br>香港 灣仔區駱克道3號22樓<br>廣州市 天河區林和東路林和村279號6棟4004單元<br>電話：86-20-3820 3233<br>郵箱：kenneth.kwan@step-one.hk',
];
