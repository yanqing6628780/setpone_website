<?php

return [
    'home' => '<a href="/">首页</a>',
    'about' => '<a href="/about">关于我们</a>',
    'contact' => '<a href="/contact">联系我们</a>',
    'breadCrumbContentTitle' => '正文',
    'lang' => 'Language',
    'news_title' => '新闻',
    'service_title' => '服务',
    'about_title' => '关于我们',
    'contact_title' => '联系我们',
    'partener_title' => '合作伙伴',
    'contact_info' => 'STEP ONE Solutions Limited <br>香港 湾仔区骆克道3号22楼<br>广州市 天河区林和东路林和村279号6栋4004单位<br>电话：86-20-3820 3233<br>邮箱：kenneth.kwan@step-one.hk',

];
