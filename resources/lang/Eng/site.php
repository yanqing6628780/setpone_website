<?php

return [
    'home' => '<a href="/Eng">Home</a>',
    'about' => '<a href="/Eng/about">About Us</a>',
    'breadCrumbContentTitle' => 'Content',
    'lang' => '语言',
    'about_title' => 'About Us',
    'contact_title' => 'Contact Us',
    'partener_title' => 'Partener',
    'contact_info' => 'STEP ONE Solutions Limited <br>7/F,FUHAI BUILDING 153 TIAN HE EAST ROAD GUANGZHOU<br>Tel：86-20-3820 3233<br>Email：kenneth.kwan@step-one.hk',
];
