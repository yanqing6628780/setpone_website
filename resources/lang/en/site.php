<?php

return [
    'home' => '<a href="/Eng">Home</a>',
    'about' => '<a href="/Eng/about">About Us</a>',
    'contact' => '<a href="/Eng/contact">Contact Us</a>',
    'breadCrumbContentTitle' => 'Content',
    'lang' => '语言',
    'news_title' => 'News',
    'service_title' => 'IT service',
    'about_title' => 'About Us',
    'contact_title' => 'Contact Us',
    'partener_title' => 'Partener',
    'contact_info' => 'STEP ONE Solutions Limited <br>22/F, 3 Lockhart Road, Wan Chai, Hong Kong<br>Room 4004, Unit 6, No. 279, Lin Hecun, LinHe Dong Road, Tianhe District, Guangzhou<br>Tel：86-20-3820 3233<br>Email：kenneth.kwan@step-one.hk',

];
