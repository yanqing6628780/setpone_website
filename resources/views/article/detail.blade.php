@extends('_layouts.default')

@section('content')
<div class="container page-main">
    <div class="row">
        <div class="col-md-3 siderbar">
            @include('_layouts.siderbar')
        </div>
        <div class="col-md-9">
            <ol class="breadcrumb">
                <li>@lang('site.home')</li>
                <li class="active">@lang('site.news_title')</li>
            </ol>
            <h3 class="text-center"><a href="blog-post.html">{{ $detail->{'title'.$currentLang} }}</a> <br> <small>{{  date("Y-m-d", strtotime($detail->publish_date))  }}</small> </h3>
            <div class="content">
            {!! $detail->{'content'.$currentLang} !!}
            </div>
        </div>
    </div>
</div>
@endsection