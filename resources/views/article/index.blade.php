@extends('_layouts.default')

@section('content')
<div class="container page-main">
    <div class="row">
        <div class="col-md-3 siderbar">
            @include('_layouts.siderbar')
        </div>
        <div class="col-md-9">
            <ol class="breadcrumb">
                <li>@lang('site.home')</li>
                <li class="active">@lang('site.news_title')</li>
            </ol>
            <ul class="list-unstyled">
            @foreach ($articles as $article)
                <li>
                    <h5>
                    <a href="{{ URL($currentLangLink.'articles/show/'.$article->id) }}">{{ $article->{'title'.$currentLang} }}</a>
                    <span class="pull-right">{{ date( 'Y-m-d', strtotime($article->{'newsDate'}) ) }}</span>
                    </h5>
                </li>
            @endforeach
            </ul>
        </div>
    </div>
</div>
@endsection