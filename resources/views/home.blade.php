@extends('_layouts.default')

@section('content')
<div class="home-slider">
    <!-- Carousel -->
    <div id="home-slider" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#home-slider" data-slide-to="0" class="active"></li>
            <li data-target="#home-slider" data-slide-to="1"></li>
            {{-- <li data-target="#home-slider" data-slide-to="2"></li> --}}
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <!-- Slide #1 -->
            <div class="item active">
                <a href="{{url('/service/show/7')}}"><img class="img-responsive" src="/assets/img/banner01.jpg" alt="..."></a>
            </div>
            <div class="item">
                <a href="{{url('/service/show/4')}}"><img class="img-responsive" src="/assets/img/banner02.jpg" alt="..."></a>
            </div>
        </div>
        <a class="carousel-arrow carousel-arrow-prev hidden-xs hidden-sm" href="#home-slider" data-slide="prev">
        <i class="fa fa-angle-left fa-2x"></i>
        </a>
        <a class="carousel-arrow carousel-arrow-next hidden-xs hidden-sm" href="#home-slider" data-slide="next">
        <i class="fa fa-angle-right fa-2x"></i>
        </a>
    </div>
</div>
<div class="Dcontent2 container">
    <div class="Dcontent2Title row hidden-xs">
        <div class="col-md-12"><img class="img-responsive" src="/assets/img/idesign_51.jpg"></div>
    </div>
    <div class="row index-service">
        @foreach ($news as $new)
        <div class="col-md-4 item">
            <div class="thumbnail">
                <h5>{{ $new->{'newsName'.$currentLang} }}</h5>
                <a href="{{ URL($currentLangLink.'service/show/'.$new->newsID) }}">
                @if ( $new->newsImage->exists() )
                    <img height="200" class="img-responsive" src="/images/{{ $new->newsImage->getPath() }}">
                @else
                    <img height="200" class="img-responsive" src="/assets/img/laptop.png">
                @endif
                </a>
                <div class="caption">
                    {!! $new->{'newsDesc'.$currentLang} !!}
                </div>
            </div>
        </div>
        @endforeach
        <div class="col-md-12">
            @if ($currentLang === "Chs")
            <h4  class="text-right"><a href="{{url('service/')}}">更多</a></h4>
            @elseif ($currentLang === "Cht")
            <h4  class="text-right"><a href="{{url('service/')}}">更多</a></h4>
            @else
            <h4  class="text-right"><a href="{{url('service/')}}">More</a></h4>
            @endif
        </div>
    </div>
</div>
<div class="container content3">
    <div class="row">
        <div class="col-md-4">
            @if ($currentLang === "Chs")
            <h2>为什么选择 Step One</h2>
            <p>Step one 擁有超過10年的IT技術解決經驗，提供全方位的IT技術支援和中小企增值服務。我們的使命在於用高品質的服務以滿足所有企業的IT需求。我們的最終目標是要建立一個有效和高效的營運環境，協助我們的客戶抓緊商機，以及在當今競爭激烈的商業環境中克服挑戰。</p>
            @elseif ($currentLang === "Cht")
            <h2>為什麽選擇 Step One</h2>
            <p>Step one 擁有超過10年的IT技術解決經驗，提供全方位的IT技術支援和中小企增值服務。我們的使命在於用高品質的服務以滿足所有企業的IT需求。我們的最終目標是要建立一個有效和高效的營運環境，協助我們的客戶抓緊商機，以及在當今競爭激烈的商業環境中克服挑戰。</p>
            @else
            <h2>Why Step One?</h2>
            <p>Step One offers full support and a broad range of services to clients of all sizes. Our customized and high quality solutions are designed to meet each client’s target budget and help our clients meet new their business standard. </p>
            @endif
        </div>
        <div class="col-md-4">
            @if ($currentLang === "Chs")
            <h2>關於 Step One Solutions</h2>
            <p>Step one 拥有超过10年的IT技术解决经验，提供全方位的IT技术支援和中小企增值服务。我们的使命在于用高品质的服务以满足所有企业的IT需求。我们的最终目标是要建立一个有效和高效的营运环境，协助我们的客户抓紧商机，以及在当今竞争激烈的商业环境中克服挑战。</p>
            @elseif ($currentLang === "Cht")
            <h2>關於 Step One Solutions</h2>
            <p>Step one 擁有超過10年的IT技術解決經驗，提供全方位的IT技術支援和中小企增值服務。我們的使命在於用高品質的服務以滿足所有企業的IT需求。我們的最終目標是要建立一個有效和高效的營運環境，協助我們的客戶抓緊商機，以及在當今競爭激烈的商業環境中克服挑戰。</p>
            @else
            <h2>About Step One Solutions?</h2>
            <p>Step one is a true full service Systems Integrator and Value-Added Reseller, with near 10 years of proven experience in IT solution. Our mission is to provide best-fit and high quality solution satisfying business IT needs. <br> Our ultimate goal is to create an effective and efficient operational environment for our customers, as it's essentially important to equip themselves for business opportunities as well as overcoming challenges in the nowadays competitive business environment.</p>
            @endif
        </div>
        <div class="col-md-4">
            @if ($currentLang === "Chs")
            <h2>认可经销商</h2>
            <img class="img-responsive" src="/wp-content/uploads/2013/01/bottom_logo2.png">
            @elseif ($currentLang === "Cht")
            <h2>認可經銷商</h2>
            <img class="img-responsive" src="/wp-content/uploads/2013/01/bottom_logo2.png">
            @else
            <h2>Authorized  reseller</h2>
            <img class="img-responsive" src="/wp-content/uploads/2013/01/bottom_logo2.png">
            @endif
        </div>
    </div>
</div>
@endsection

@section('page_js')
<script type="text/javascript">
$(function(){
    $('.index-service .item:nth-child(3n)').addClass('no-border');
})
</script>
@endsection