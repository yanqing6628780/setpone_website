<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="ico/favicon.ico">

<title>Step One</title>

<!-- Bootstrap core CSS -->
<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/font-awesome.min.css" rel="stylesheet">
<link href="/assets/css/animate.css" rel="stylesheet">
<link href="/assets/css/lightbox.css" rel="stylesheet">
<link href='http://fonts.useso.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
<link href="/assets/css/style.css" rel="stylesheet">
<link href="/assets/css/index.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<!-- Navigation -->
<div class="navbar stepOne-navbar" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="DHeadLogoimg navbar-brand" href="/"><img src="/assets/img/logo.png"></a>
        </div>
        <div class="collapse navbar-collapse"  id="navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="">@lang("site.home")</li>
                <li class=""><a href="{{url($currentLangLink.'articles')}}">@lang("site.news_title")</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">@lang("site.service_title")</a>
                    <ul class="dropdown-menu">
                        @foreach ($lastServices as $new)
                        <li><a href="{{ URL($currentLangLink.'service/show/'.$new->newsID) }}">{{ $new->{'newsName'.$currentLang} }}</a></li>
                        @endforeach
                    </ul>
                </li>
                <li class="">@lang("site.about")</li>
                <li class="">@lang("site.contact")</li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">@lang("site.lang")</a>
                    <ul class="dropdown-menu">
                        <li><a href="/locale/Chs">简体</a></li>
                        <li><a href="/locale/Cht">繁體</a></li>
                        <li><a href="/locale/Eng">English</a></li>
                    </ul>
                </li>
            </ul>
        </div>
     </div>
 </div>
<!-- Navigation -->

<div class="wrapper">
@yield('content')
</div>
<!-- Footer -->
<div class="foot-chi"></div>
<div class="Bottom">
    <div class="Bottomcontent">
        <div class="container">
            <div class="row">
                <div class="Bottomcontentimg col-xs-12 col-sm-3"><img class="img-responsive" src="/assets/img/footlogo.png"></div>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-xs-6 col-sm-4">
                            <div class="box-2">
                                <h4>@lang("site.about") </h4>
                                <h4>@lang("site.contact") </h4>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4">
                            <h4>@lang('site.service_title')</h4>
                            <ul class="list-unstyled">
                                @foreach ($lastServices as $new)
                                <li><a href="{{ URL($currentLangLink.'service/show/'.$new->newsID) }}">{{ $new->{'newsName'.$currentLang} }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <h4>@lang("site.contact_title")</h4>
                            <p>@lang("site.contact_info") </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="Bottomcontentfont1">
        Copyright© 2015 Step-one.hk. All Rights Reserved. 
        </div>
    </div>
</div>
<!-- JavaScript -->
<script src="http://cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $(".wrap5 div").hover(function() {
        $(this).animate({"top": "-131px"}, 440, "swing");
    },function() {
        $(this).stop(true,false).animate({"top": "0px"}, 250, "swing");
    });
        $(".wrap2 div").hover(function() {
        $(this).animate({"top": "-78px"}, 440, "swing");
    },function() {
        $(this).stop(true,false).animate({"top": "0px"}, 250, "swing");
    });
});
</script>
@yield('page_js')
</body>
</html>