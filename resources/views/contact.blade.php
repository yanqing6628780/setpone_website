@extends('_layouts.default')

@section('content')
<div class="container page-main">
    <div class="row">
        <div class="col-md-3 siderbar">
            @include('_layouts.siderbar')
        </div>
        <div class="col-md-9">
            <ol class="breadcrumb">
                <li>@lang('site.home')</li>
                <li class="active">@lang('site.about_title')</li>
            </ol>
            <h3 class="text-center">
                @if ($currentLang === "Chs")
                联系我们 
                @elseif ($currentLang === "Cht")
                聯繫我們
                @else
                Contact Step One
                @endif
            </h3>
            <div class="content">
                @if ($currentLang === "Chs")
                    
                    <h2>与 Step One IT Solution 取得联系</h2>

                    <p>如果您想与我们联系，或有任何疑问，您可以直接联络我们或者请填写此表格，我们将会尽快回覆。</p>

                    <p><strong>联络资料</strong><br>
                    电话： +86-20-3820 3233<br>
                    电邮：邮箱：kenneth.kwan@step-one.hk<br>
                    地址：9/F., Henan Building,19 Luad Road, Wan Chai, Hong Kong</p>


                @elseif ($currentLang === "Cht")
                    <h2>与 Step One IT Solution 取得联系</h2>

                    <p>如果您想与我们联系，或有任何疑问，您可以直接联络我们或者请填写此表格，我们将会尽快回覆。</p>

                    <p><strong>聯絡資料</strong><br>
                    電話：+86-20-3820 3233<br>
                    電郵：kenneth.kwan@step-one.hk<br>
                    地址：9/F., Henan Building,19 Luad Road, Wan Chai, Hong Kong</p>

                @else
                    <h2><em>Get in touch with Step One IT Solution ?</em></h2>

                    <p>If you would like to get in touch with us or have any queries, You can contact us or please fill out this form and we will get back to as soon as possible.</p>

                    <p><strong>Contact Information</strong><br>
                    Tel: +86-20-3820 3233<br>
                    Email: kenneth.kwan@step-one.hk<br>
                    Address: 9/F., Henan Building,19 Luad Road, Wan Chai, Hong Kong</p>            
                @endif
            </div>
        </div>
    </div>
</div>
@endsection