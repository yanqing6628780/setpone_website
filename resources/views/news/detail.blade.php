@extends('_layouts.default')

@section('content')
<div class="container page-main">
    <div class="row">
        <div class="col-md-3 siderbar">
            @include('_layouts.siderbar')
        </div>
        <div class="col-md-9">
            <ol class="breadcrumb">
                <li>@lang('site.home')</li>
                <li class="active">@lang('site.breadCrumbContentTitle')</li>
            </ol>
            <h3 class="text-center"><a href="blog-post.html">{{ $detail->{'newsName'.$currentLang} }}</a> <br> <small>{{  date("Y-m-d", strtotime($detail->newsDate))  }}</small> </h3>
            <div class="content">
            {!! $detail->{'newsContents'.$currentLang} !!}
            </div>
        </div>
    </div>
</div>
@endsection