@extends('_layouts.default')

@section('content')
<div class="container page-main">
    <div class="row">
        <div class="col-md-3 siderbar">
            @include('_layouts.siderbar')
        </div>
        <div class="col-md-9">
            <ol class="breadcrumb">
                <li>@lang('site.home')</li>
                <li class="active">@lang('site.service_title')</li>
            </ol>
            <ul class="list-unstyled">
            @foreach ($result as $new)
                <li> 
                    <h5>
                        <a href="{{ URL($currentLangLink.'service/show/'.$new->newsID) }}">{{ $new->{'newsName'.$currentLang} }}</a>
                        <span class="pull-right">{{ date( 'Y-m-d', strtotime($new->{'newsDate'}) ) }}</span>
                    </h5>
                </li>
            @endforeach
            </ul>
        </div>
    </div>
</div>
@endsection