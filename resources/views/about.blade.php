@extends('_layouts.default')

@section('content')
<div class="container page-main">
    <div class="row">
        <div class="col-md-3 siderbar">
            @include('_layouts.siderbar')
        </div>
        <div class="col-md-9">
            <ol class="breadcrumb">
                <li>@lang('site.home')</li>
                <li class="active">@lang('site.about_title')</li>
            </ol>
            <h3 class="text-center">
                @if ($currentLang === "Chs")
                关于 Step One 
                @elseif ($currentLang === "Cht")
                關於 Step One 
                @else
                About Step One
                @endif
            </h3>
            <div class="content">
                @if ($currentLang === "Chs")

                    <p>Step one 拥有超过10年的IT技术解决经验，提供全方位的IT技术支援和中小企增值服务。我们的使命在于用高质量的服务以满足所有企业的IT需求。我们的最终目标是要建立一个有效和高效的营运环境，协助我们的客户抓紧商机，以及在当今竞争激烈的商业环境中克服挑战。</p>

                    <p>&nbsp;</p>

                    <p><strong>我们提供什么？</strong></p>

                    <p>&nbsp;</p>

                    <p>Step One 提供全面的支援服务给各种规模的客户。透过高品质的服务，我们有信心可以满足每个客户的目标预算，并帮助每一位客户提升业务水平。</p>

                    <p>&nbsp;</p>

                    <p>&gt;&nbsp; 系统整合服务</p>

                    <p>&gt;&nbsp; 系统安全建立及配置</p>

                    <p>&gt;&nbsp; 办公室维护</p>

                    <p>&gt;&nbsp; 软硬件配置</p>

                    <p>&gt;&nbsp; 设计与技术执行</p>

                    <p>&gt;&nbsp; 授权经销商</p>

                    <p>&nbsp;</p>

                    <p>我们很荣幸被获邀授权代理以下品牌。无论是一台笔记本计算机，服务器，软件或工作站，我们都可以为你提供。我们亦拥有大量的配套及资源，可以为你于售前或售后提供全面的协助。</p>

                    <p>&nbsp;</p>

                    <p>我们亦可于任何地方为我们的客户提供服务或培训，包括系统复原、系统培训、网络整合等。</p>

                    <p>&nbsp;</p>

                    <p>&nbsp;</p>

                    <p>&nbsp;</p>

                @elseif ($currentLang === "Cht")

                    <p>Step one 擁有超過10年的IT技術解決經驗，提供全方位的IT技術支援和中小企增值服務。我們的使命在於用高品質的服務以滿足所有企業的IT需求。我們的最終目標是要建立一個有效和高效的營運環境，協助我們的客戶抓緊商機，以及在當今競爭激烈的商業環境中克服挑戰。</p>

                    <p>&nbsp;</p>

                    <p><strong>我們提供什麼？</strong></p>

                    <p>&nbsp;</p>

                    <p>Step One 提供全面的支援服務給各種規模的客戶。透過高品質的服務，我們有信心可以滿足每個客戶的目標預算，並幫助每一位客戶提昇業務水平。</p>

                    <p>&nbsp;</p>

                    <p>&gt;&nbsp; 系統整合服務</p>

                    <p>&gt;&nbsp; 系統安全建立及配置</p>

                    <p>&gt;&nbsp; 辦公室維護</p>

                    <p>&gt;&nbsp; 軟硬件配置</p>

                    <p>&gt;&nbsp; 設計與技術執行</p>

                    <p>&gt;&nbsp; 授權經銷商</p>

                    <p>&nbsp;</p>

                    <p>我們很榮幸被獲邀授權代理以下品牌。無論是一台筆記本電腦，服務器，軟件或工作站，我們都可以為你提供。我們亦擁有大量的配套及資源，可以為你於售前或售後提供全面的協助。</p>

                    <p>&nbsp;</p>

                    <p>我們亦可於任何地方為我們的客戶提供服務或培訓，包括系統復原、系統培訓、網絡整合等。</p>

                @else

                    <p><strong><em>Step one is a true full service Systems Integrator and Value-Added Reseller, with near 10 years of proven experience in IT solution. Our mission is to provide best-fit and high quality solution satisfying business IT needs.</em></strong></p>

                    <p><strong><em>Our ultimate goal is to create an effective and efficient operational environment for our customers, as it&#39;s essentially important to equip themselves for business opportunities as well as overcoming challenges in the nowadays competitive business environment.</em></strong></p>

                    <p><em>What we provide ?</em></p>

                    <p><strong><em>Step One offers full support and a broad range of services to clients of all sizes. Our customized and high quality solutions are designed to meet each client&rsquo;s target budget and help our clients meet new their business standard.</em></strong></p>

                    <p>&nbsp;</p>

                    <p>&gt;<strong><em>System Integration Services</em></strong></p>

                    <p>&gt;<strong><em>System Security Services</em></strong></p>

                    <p>&gt;<strong><em>Office Maintenance Service</em></strong></p>

                    <p>&gt;<strong><em>Hardware and software providing</em></strong></p>

                    <p>&gt;<strong><em><strong><em>Design and Implementation Services</em></strong></em></strong></p>

                    <p>&nbsp;</p>

                    <p><strong><em>Authorized Reseller</em></strong></p>

                    <p><strong><em>We are pound to be authorized for below brands. Any products you may be interested in be it a laptop, server, software or workstation, we can provide them to you. We also have access to all resources so that we can adequately support you both before and after the sales.</em></strong></p>

                    <p><strong><em>We also offer service, insight, training, and support wherever to our customers. We provide everything from telephone support to disaster recovery, industry-specific training, networking and integration, flexible purchase options, and technical expertise.</em></strong></p>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection