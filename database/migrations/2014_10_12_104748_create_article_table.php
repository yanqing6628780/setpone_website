<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArticleTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('titleEng', 100);
            $table->string('titleCht', 100)->nullable();
            $table->string('titleChs', 100)->nullable();
            $table->string('article_img', 128)->nullable();
            $table->date('publish_date')->nullable();
            $table->text('contentEng');
            $table->text('contentCht')->nullable();
            $table->text('contentChs')->nullable();
            $table->smallInteger('ordering');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('article');
    }

}
