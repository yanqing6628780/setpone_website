<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNewsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function(Blueprint $table)
        {
            $table->increments('newsID');
            $table->string('newsNameEng', 100);
            $table->string('newsNameCht', 100)->nullable();
            $table->string('newsNameChs', 100)->nullable();
            $table->string('newsImage', 100)->nullable();
            $table->date('newsDate')->nullable();
            $table->text('newsContentsEng');
            $table->text('newsContentsCht')->nullable();
            $table->text('newsContentsChs')->nullable();
            $table->smallInteger('ordering');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news');
    }

}
