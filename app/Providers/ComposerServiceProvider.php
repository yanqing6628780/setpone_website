<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\news;
use App;
use Config;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using Closure based composers...
        view()->composer('*', function ($view) {
            $languages_config = Config::get('app.alt_langs');
            $currentLang = App::getLocale();
            $currentLangLink = App::getLocale() != $languages_config[0] ? App::getLocale().'/' : '';
            $news = news::orderBy('newsDate', 'Desc')->take(3)->get();

            $view->with('lastServices', $news);
            $view->with('currentLang', $currentLang);
            $view->with('currentLangLink', $currentLangLink);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}