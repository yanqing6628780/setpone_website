<?php
Admin::model(\App\news::class)
    ->columns(function ()
{
    Column::image('newsImage', '图片');
    Column::string('newsNameEng', '标题');
    Column::date('newsDate', '日期')->formatDate('full');
})
    ->form(function ()
{
    FormItem::text('newsNameEng', 'English-Title');
    FormItem::text('newsNameCht', '繁體-標題');
    FormItem::text('newsNameChs', '简体-标题');
    FormItem::date('newsDate', '日期');
    FormItem::image('newsImage', '图片');
    FormItem::ckeditor('newsDescEng', 'English-Desc');
    FormItem::ckeditor('newsDescCht', '繁體-摘要');
    FormItem::ckeditor('newsDescChs', '简体-摘要');
    FormItem::ckeditor('newsContentsEng', 'English-content');
    FormItem::ckeditor('newsContentsCht', '繁體-內容');
    FormItem::ckeditor('newsContentsChs', '简体-内容');
});
?>