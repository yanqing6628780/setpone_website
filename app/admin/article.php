<?php
Admin::model(\App\article::class)
    ->columns(function ()
{
    Column::image('article_img', '图片');
    Column::string('titleEng', '标题');
    Column::date('publish_date', '日期')->formatDate('full');
})
    ->form(function ()
{
    FormItem::text('titleEng', 'English-Title');
    FormItem::text('titleCht', '繁體-標題');
    FormItem::text('titleChs', '简体-标题');
    FormItem::date('publish_date', '日期');
    FormItem::image('article_img', '图片');
    FormItem::ckeditor('contentEng', 'English-content');
    FormItem::ckeditor('contentCht', '繁體-內容');
    FormItem::ckeditor('contentChs', '简体-内容');
});
?>