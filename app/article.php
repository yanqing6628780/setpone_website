<?php
namespace App;

use SleepingOwl\Models\SleepingOwlModel;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;

class article extends SleepingOwlModel implements ModelWithImageFieldsInterface
{
    protected $table = 'article';
    protected $guarded = ['id'];

    //改变默认排序
    public function scopeDefaultSort($query)
    {
        return $query->orderBy('publish_date', 'Asc');
    }

    //日期处理为数据库格式
    public function getDates()
    {
        return array_merge(parent::getDates(), ['publish_date']);
    }

    public function getImageFields()
    {
        return [
            'article_img' => 'article/',
        ];
    }
}
