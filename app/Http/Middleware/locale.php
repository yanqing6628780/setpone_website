<?php 

namespace App\Http\Middleware;

use Closure;
use Session;
use App;
use Config;

class locale {

    /**
     * The availables languages.
     *
     * @array $languages
     */
    protected $languages;

    public function __construct()
    {
        $this->languages = Config::get('app.alt_langs');
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = Session::get('locale', $this->languages[0]);
        App::setLocale($locale);

        return $next($request);
    }

}
 ?>