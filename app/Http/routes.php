<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/locale/{locale?}', 'LocaleController@setLocale');

//判断第一个参数是否为语言参数
$locale = in_array(Request::segment(1), Config::get('app.alt_langs')) ? Request::segment(1) : null;
Route::group([
    'middleware' => 'locale',
    'prefix' => $locale
], function() {
    Route::get('/', 'HomeController@index');
    Route::controller('service', 'ServicesController');
    Route::controller('articles', 'articleController');
    Route::get('about', function() {
        return view("about");
    });
    Route::get('contact', function() {
        return view("contact");
    });
});