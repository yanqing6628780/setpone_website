<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use App;
use App\news;
use Session;

class ServicesController extends BaseController
{

    public function getIndex()
    {
        $data['result'] = news::all();
        return view('news.index', $data);
    }

    public function getShow($id)
    {
        $data['detail'] = news::where('newsID', $id)->firstOrFail();
        return view('news.detail', $data);
    }
}
