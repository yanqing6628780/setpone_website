<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use URL;
use Session;
use Config;

/*
*   语言设置将数据保存在session中
*/
class LocaleController extends Controller {

    protected $languages;

    public function __construct()
    {
        $this->languages = Config::get('app.alt_langs');
    }

    public function setLocale($locale)
    {
        if (! in_array($locale,$this->languages))
        {
            $locale = $this->languages[0];
        }
        Session::put('locale', $locale);

        return redirect( url('/', $locale) );
    }
}