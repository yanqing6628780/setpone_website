<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use App;
use App\article;
use Session;

class articleController extends BaseController
{
    public function getIndex()
    {
        $data['articles'] = article::all();
        return view('article.index', $data);
    }

    public function getShow($id)
    {
        $data['detail'] = article::where('id', $id)->firstOrFail();
        return view('article.detail', $data);
    }
}
