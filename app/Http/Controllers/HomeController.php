<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use App;
use Session;
use Request;
use App\news;

class HomeController extends BaseController
{
    public function index()
    {
        $data['news'] = news::take(3)->get();
        return view('home', $data);
    }
}
