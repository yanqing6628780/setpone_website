<?php
namespace App;

use SleepingOwl\Models\SleepingOwlModel;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;

class news extends SleepingOwlModel implements ModelWithImageFieldsInterface
{

    protected $table = 'news';
    protected $primaryKey = 'newsID';
    protected $guarded = ['newsID'];

    //改变默认排序
    public function scopeDefaultSort($query)
    {
        return $query->orderBy('newsDate', 'Asc');
    }

    //日期处理为数据库格式
    public function getDates()
    {
        return array_merge(parent::getDates(), ['newsDate']);
    }

    public function getImageFields()
    {
        return [
            'newsImage' => 'news/',
        ];
    }
}
